<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Creative Services, College of William and Mary
*/
if (! function_exists('printGAevent')) {
	function printGAevent($type, $channel, $linkTitle) {
		echo 'onClick="_gaq.push([\'_trackEvent\', \''.$type.'\', \''.$channel.'\', \''.$linkTitle.'\']);"';
	}
}

if (! function_exists('printAccount')) {
	function printAccount($e, $smUrls, $smtypeOptions, $thisPage) {
		foreach ($e["accounts"] as $act) {
				$url = $smUrls[$act["smtype_id"]].$act["username"];
				if ($act["smtype_id"] == 7) {
					$url .= ".tumblr.com/";
				}
				echo '<li><a href="'.$url.'" target="_blank" title="'.$e["name"];
				if (!empty($act["title"])) {
					echo ' - '.$act["title"];
				}
				if ($smtypeOptions[$act["smtype_id"]] != "Blog") {
					echo ' on';
				}
				echo ' '.$smtypeOptions[$act["smtype_id"]].'" ';
				printGAevent($thisPage, $smtypeOptions[$act["smtype_id"]], $e["name"]);
				echo '><img src="'.base_url('/img/icons/'.strtolower($smtypeOptions[$act["smtype_id"]]).'.png').'" alt="'.$smtypeOptions[$act["smtype_id"]].' icon"/></a></li>';
			}
	}
}

/* End of file display_helper.php */
