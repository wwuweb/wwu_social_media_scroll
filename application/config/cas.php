<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['cas_server_url'] = 'https://websso.wwu.edu/cas';
$config['phpcas_path'] = FCPATH.'assets/CAS-1.3.3';
$config['cas_disable_server_validation'] = TRUE;