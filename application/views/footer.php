
<footer class="site-footer" role="contentinfo">
  <div class="wrap">

    <div class="footer-left">
      <div style="text-align: center;" class="campaign-banner">
        <div class="amcl">Active Minds Changing Lives</div>
      </div>
    </div>

    <div class="footer-center">
      <ul class="menu">
        <li><a href="http://wwu.edu/campus-safety-resources" class="menu__link">Campus Safety Resources</a></li>
        <li><a href="https://www.wwu.edu/files/2018-12/Social%20Media%20Guidelines%202018.pdf" class="menu__link">Social Media Guidelines</a></li>
        <li><a href="http://www.wwu.edu/brand/" class="menu__link">Branding</a></li>
        <li><a href="http://wwu.edu/commitment-accessibility">Accessibility</a></li>
      </ul>
    </div>

    <div class="footer-right" role="complementary">
     <div class="western-card">
      <h1><a href="//www.wwu.edu">Western Washington University</a></h1>
      <div class="western-contact-info">
        <p class="western-address">516 High Street<br>
          Bellingham, WA 98225</p>
          <p class="western-telephone"><a href="tel:3606503000">(360) 650-3000</a></p>
          <p class="western-contact"><a href="//www.wwu.edu/wwucontact/">Contact Western</a></p>
          </div>
        <div class="western-social-media">
          <ul>
            <li class="western-facebook-icon"><a href="//www.facebook.com/westernwashingtonuniversity">Facebook</a></li>
            <li class="western-flickr-icon"><a href="//www.flickr.com/wwu">Flickr</a></li>
            <li class="western-twitter-icon"><a href="//twitter.com/#!/WWU">Twitter</a></li>
            <li class="western-youtube-icon"><a href="//www.youtube.com/wwu">Youtube</a></li>
            <li class="western-googleplus-icon"><a href="//plus.google.com/+wwu/posts">Google Plus</a></li>
            <li class="western-rss-icon"><a href="//news.wwu.edu/go/feed/1538/ru/atom/">RSS</a></li>
          </ul>
        </div>
      </div>
    </div>

  </div> <!-- end div.footer-wrapper -->
</footer>

</div> <!-- end container -->
  <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/western.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>

	<?php include("google_analytics.html"); ?>
</body>
</html>
