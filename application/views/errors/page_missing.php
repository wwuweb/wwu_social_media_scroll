<?php $this->load->view("header.php"); ?>
<div id="page_missing">
	<h3>That web page doesn't exist (404)</h3>
	<p>Looks like a page has moved or you followed a bad link.</p>
</div>
<?php $this->load->view("footer.php"); ?>