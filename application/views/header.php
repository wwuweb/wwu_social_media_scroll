<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="script-src 'self' 'unsafe-inline' http://twemoji.maxcdn.com http://cdnjs.cloudflare.com https://npmcdn.com http://www.google-analytics.com;">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.ico" type="image/x-icon"/>
    <link href="<?php echo base_url(); ?>css/style.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/adminlte-plugins/jQuery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="//twemoji.maxcdn.com/2/twemoji.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.0/jquery-migrate.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>  
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/2.1.0/jquery.infinitescroll.js"></script>

</head>
<body>
  <header role="banner">
    <section class="western-header" aria-label="University Links, Search, and Navigation">
      <div class="center-content">
        <a class="western-logo" href="http://www.wwu.edu">Western Washington University</a>
        <a class="mobile-logo" href="http://www.wwu.edu"></a>
        <a href="<?php echo site_url() ?>" class="site-name">#WWUSocial</a>
        <nav role="navigation" aria-label="WWU menu">
          <div class="western-quick-links" aria-label="Western Quick Links">
            <ul>
              <li><a href="//www.wwu.edu/academic_calendar" title="Calendar">
                <span>Calendar</span>
              </a></li>
              <li><a href="//www.wwu.edu/directory" title="Directory">
                <span>Directory</span>
              </a></li>
              <li><a href="//www.wwu.edu/index" title="Index">
                <span>Index</span>
              </a></li>
              <li><a href="//www.wwu.edu/campusmaps" title="Map">
                <span>Map</span>
              </a></li>
              <li><a href="//mywestern.wwu.edu" title="myWestern">
                <span>myWestern</span>
              </a></li>
            </ul>
          </div>
          <div class="western-search" role="search" aria-label="University and Site Search">
            <button role="button" aria-pressed="false" aria-label="Open the search box"><span class="acc-hide-text">Open Search</span></button>
            <div class="western-search-widget">
              <form id="western-search-widget-form" method="get" action="https://search2.wwu.edu/search">
                <div class="western-search-terms">
                  <input type="search" id="searchfield" name="q" size="17" maxlength="50" placeholder="Search all WWU sites..." aria-label="Search all WWU sites..." autocomplete="off"> 
                  <input type="submit" size="6" value="Search" aria-label="Search button"> 
                </div>
              <input type="hidden" name="client" value="default_frontend">
              <input type="hidden" name="output" value="xml_no_dtd">
              <input type="hidden" name="proxystylesheet" value="default_frontend">
              <input type="hidden" name="sort" value="date:D:L:d1">
              <input type="hidden" name="oe" value="UTF-8">
              <input type="hidden" name="ie" value="UTF-8">
              <input type="hidden" name="ud" value="1">
              <input type="hidden" name="exclude_apps" value="1">
              <input type="hidden" name="filter" value="0"/>

              <!-- Change the "sharename" to match your search "Collection" name. Contact web.help@wwu.edu for help. -->
              <input type="hidden" name="site" value="">
<!--
              <label for="submit-search" class="screen-reader-text">Search</label>
              <input id="submit-search" class="wp-wwu-search" name="wp-wwu-search" value="Search" type="submit">
-->
            </form> <!--end western-search-widget-form -->
          </div> <!-- end western-search-widget -->
        </div>
      </nav>
      <?php 
      if ($active == "scroll") {
        echo sprintf("<a class=\"sm-nav-link\" href=\"%s\">%s</a>", site_url("sm_directory"), "Social Media Directory >>");
      }
      else if($active == "directory") {
        echo sprintf("<a class=\"sm-nav-link\" href=\"%s\">%s</a>", site_url(), "<< Back to Social Stream");
      }
      ?>
    </div> <!-- end center content -->
  </section>
</header> <!-- end header -->
<div id="container">


