<?php
$this->load->view("manage/header.php");
?>
<div class="box box-solid">
    <?php
	    echo '<div class="box-header">';
        if ($account["id"] == 0) {
            echo '<h3 class="box-title">Add New Account</h3>';
        } else {
            echo '<h3 class="box-title">Edit Existing Account</h3>';
        }
		echo "</div><div class='box-body'>\n<div class=\"form-group\">";
        echo form_open('manage/saveaccount/'.$account['id'], array("id"=>"entryform","name"=>"entryform"));
		echo form_hidden('id', $account["id"]);
		echo form_label("Organization", "entity_id", array("class"=>"requiredField"));
		echo form_dropdown('entity_id', $entityOptions, set_value('entity_id',$account['entity_id']), array('class' => 'form-control'));
		echo '</div>';
        echo "\n<div class=\"form-group\">";
		echo form_label("Social Media Type", "smtype_id", array("class"=>"requiredField"));
		echo form_dropdown('smtype_id', $smtypeOptions, set_value('smtype_id',$account['smtype_id']), array('class' => 'form-control'));
		echo '</div>';
        echo "\n<div class=\"form-group\">";
        echo form_label("Username<br/><em>(as seen in the accounts URL)</em>", "username", array("class"=>"requiredField"));
		echo form_input(array(
						'name' => 'username',
						'id' => 'username',
						'class' => 'form-control',
						'value' => set_value('username', $account['username']),
                        'length' => 256,
                        'size' => 64
						));
        echo '</div>';
        echo "\n<div class=\"form-group\">";
        echo form_label("Title<br/><em>(if different than Organization)</em>", "title", array("class"=>"requiredField"));
		echo '<input type="text" name="title" id="title" class="form-control" value="'.set_value('title', $account['title']).'" length="256" size="64"/>';
        echo '</div>';
        echo "\n<div class=\"form-group\">";
        echo form_label("Latest Activity", "last_updated", array("class"=>"requiredField"));
		echo form_input(array(
						'name' => 'last_updated',
						'id' => 'last_updated',
						'class' => 'form-control',
						'value' => set_value('last_updated', $account['last_updated']),
                        'length' => 256,
                        'type' => 'date',
                        'size' => 64
						));
        echo '</div>';
		echo "\n<div class=\"form-group\">";
		echo form_label("Account Status", "status", array("class"=>"requiredField"));
		echo form_dropdown('status', $accountStatusOptions, set_value('status',$account['status']), array('class' => 'form-control'));
		echo '</div>';
		echo "\n<fieldset><legend>Review Status</legend>";
		echo "\n<div class=\"form-group\">";
		echo form_label("Flagged for Review", "flagged", array("class"=>"requiredField"));
		echo form_dropdown('flagged', array(0=>"No", 1=>"Yes"), set_value('flagged',$account['flagged']), array('class' => 'form-control'));
		echo '</div>';	
		echo "\n<div class=\"form-group\">";
        echo form_label("Ticket ID", "ticket", array("class"=>"requiredField"));
		echo form_input(array(
						'name' => 'ticket',
						'id' => 'ticket',
						'class' => 'form-control',
						'value' => set_value('ticket', $account['ticket']),
                        'length' => 256,
                        'size' => 64
						));
        echo '</div>';
		echo "\n<div class=\"form-group\">";
        echo form_label("Comments", "comments", array("class"=>"requiredField"));
		echo '<textarea name="comments" id="comments" class="form-control" rows="8" cols="64">'.set_value('comments', $account['comments']).'</textarea>';
        echo '</div>';
		echo "\n</fieldset>";
        echo '<input type="submit" name="Save" value="Save" class="btn btn-primary">';
        echo form_close();
    ?>
    </div>
    </div>
    <p><a class="deleteThis btn bg-red" href="<?=site_url('manage/deleteAccount/'.$account["id"].'/'.$account['entity_id'])?>"><span class="fa fa-remove"></span> Delete This Account</a></p>

<?php
$this->load->view("manage/footer.php");
?>
