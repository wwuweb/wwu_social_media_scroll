<?php
$this->load->view("manage/header.php");
?>
<h4><?php echo $current_sm_type; ?></h4>
<?php if (!empty($slogging_accounts)) { ?>
<table class="sortableTable"><thead><tr><th>Username</th><th>Last Updated</th></tr></thead>
<tbody>
    <?php
   foreach ($slogging_accounts as $sInfo) {
	   echo '<tr><td>'.$sInfo["username"].'</td><td>'.$sInfo["lastUpdate"].'</td></tr>';
   }
    ?>
</tbody>
</table>
<?php } ?>
<?php
if (!empty($miscAccounts)) { ?>
<h3>Unknown accounts (likely Facebook groups)</h3>
<ul>
<?php
	foreach ($miscAccounts as $a) {
		echo '<li><a href="http://www.facebook.com/'.$a['username'].'">'.$a['username'].'</a></li>';
	}
	echo '</ul>';
}    
?>

<p><a href="<?php echo site_url('statistics/runSloggers/'.$current_type.'/'.$current_pg.'/'.$per_pg); ?>" class="btn btn-app"><i class="fa fa-arrow-right"></i> Next Page</a></p>
<?php
$this->load->view("manage/footer.php");
?>
