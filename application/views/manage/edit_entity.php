<?php
$this->load->view("manage/header.php");
?>
<div class="box box-solid">
	<div class="box-body">
    <?php
        echo form_open('manage/saveentity/'.$entity['id'], array("id"=>"entryform","name"=>"entryform"));
		echo form_hidden('id', $entity["id"]);
        echo "\n<div class=\"form-group\">";
        echo form_label("Name", "name", array("class"=>"requiredField"));
		echo '<input type="text" name="name" id="name" class="form-control" length="256" size="64" value="'.set_value('name', $entity['name']).'"/>';
        echo '</div>';
        echo "\n<div class=\"form-group\">";
		echo form_label("Account Type", "type", array("class"=>"requiredField"));
		echo form_dropdown('type', $typeOptions, set_value('type',$entity['type']), array('class' => 'form-control'));
		echo '</div>';
        echo "\n<div class=\"form-group\">";
		echo form_label("Category", "category_id", array("class"=>"requiredField"));
		echo form_dropdown('category_id', $categoryOptions, set_value('category_id',$entity['category_id']), array('class' => 'form-control'));
		echo '</div>';
		echo "\n<div class=\"form-group\">";
		echo form_label("School", "school_id", array("class"=>"requiredField"));
		echo form_dropdown('school_id', $schoolOptions, set_value('school_id',$entity['school_id']), array('class' => 'form-control'));
		echo '</div>';
		echo "\n<div class=\"form-group\">";
        echo form_label("Website", "website", array("class"=>"requiredField"));
		echo '<input type="text" name="website" id="website" class="form-control" value="'.set_value('website', $entity['website']).'" length="256" size="64"/>';
        echo '</div>';
		echo "\n<div class=\"form-group\">";
        echo form_label("Contact", "contact", array("class"=>"requiredField"));
		echo '<input type="text" name="contact" id="contact" class="form-control" value="'.set_value('contact', $entity['contact']).'" length="256" size="64"/>';
        echo '</div>';
        echo '<input type="submit" name="Save" value="Save" class="btn btn-primary">';
        echo form_close();
    ?>
	</div>
</div>
    <p>
    	<a class="deleteThis btn bg-red" href="<?=site_url('manage/deleteEntity/'.$entity["id"])?>"><span class="fa fa-remove"></span> Delete This Organization</a>
    </p>
<?php
$this->load->view("manage/footer.php");
?>
