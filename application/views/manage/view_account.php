<?php
$this->load->view("manage/header.php");
?>
<div class="box box-solid"><div class='box-body'>
    <?php
	    echo '<a class="btn pull-right" href="'.site_url('manage/editAccount/'.$account["id"]).'" target="_blank"><span class="glyphicon glyphicon-pencil"></span> edit</a>';
		echo '<strong>Organization</strong>: <a href="'.site_url('manage/viewEntity/'.$entity["id"]).'">'.$entity["name"].'</a><br/>';
		echo '<strong>Account Type</strong>: '.$smtypeOptions[$account['smtype_id']];
		if (!empty($account['title'])) echo ' - '.$account['title'];
		echo '<br/><a href="'.$smUrls[$account['smtype_id']].$account['username'].'" target="_blank">'.$smUrls[$account['smtype_id']].$account['username'].'</a><br/>';
		echo '<strong>Status</strong>: '.$accountStatusOptions[$account['status']];
		echo '<p>';
		if ($account['ticket'] != 0) {
			echo '<a href="'.TICKET_URL.$account['ticket'].'">Ticket '.$account['ticket'].'</a><br/>';
		}
		echo '<strong>Latest Activity</strong>: '.$account["last_updated"];
		if (!empty($account['comments'])) {
			echo '<em>'.$account['comments'].'</em></p>';
		};
		echo '</p>';
    ?>
</div></div>
<?php
$this->load->view("manage/footer.php");
?>
