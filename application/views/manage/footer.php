</section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- Default to the left -->
        <strong><?php echo INSTITUTION_NAME; ?></strong>
      </footer>
<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url("assets/adminlte-plugins/jQuery/jquery-2.1.4.min.js"); ?>" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
    <!-- Isotope -->
    <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url("assets/adminlte-dist/js/app.min.js"); ?>" type="text/javascript"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url("assets/adminlte-plugins/datatables/jquery.dataTables.min.js"); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url("assets/adminlte-plugins/datatables/dataTables.bootstrap.min.js"); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url("assets/adminlte-plugins/slimScroll/jquery.slimscroll.min.js"); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url("js/manage.js"); ?>" type="text/javascript"></script>
</body>
</html>
