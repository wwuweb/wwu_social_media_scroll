<?php
$this->load->view("manage/header.php");
?>
<h2>General Directory Statistics</h2>
<div class="box box-solid">
	<div class="box-header"><h3 class="box-title">Organizations</h3></div>
	<div class='box-body'>
    <?php
		echo '<ul>';
		$totalCt = 0;
		foreach ($entities as $eType => $eCt) {
			echo '<li>'.$eType.': '.$eCt.'</li>';
			$totalCt += $eCt;
		}
		echo '</ul>';
		echo '<p><strong>Total: '.$totalCt.'</strong></p>';
		?>
	</div>
</div>
<div class="box box-solid">
	<div class="box-header"><h3 class="box-title">Social Media Accounts</h3></div>
	<div class='box-body'>
		<?php
		echo '<ul>';
		$totalCtActive = 0;
		$totalCtInactive = 0;
		foreach ($accounts as $aType => $aCt) {
			echo '<li>'.$aType.': '.$aCt["active"].' (active), '.$aCt["inactive"].' (inactive)</li>';
			$totalCtActive += $aCt["active"];
			$totalCtInactive += $aCt["inactive"];
		}
		echo '</ul>';
		echo '<p><strong>Total Active: '.$totalCtActive.'<br/>Total Inactive: '.$totalCtInactive.'</p>';
    ?>
	</div>
</div>
<?php
$this->load->view("manage/footer.php");
?>
