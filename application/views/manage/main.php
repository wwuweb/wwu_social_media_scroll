<?php
$this->load->view("manage/header.php");
?>

    <?php
    echo '<div class="grid" data-isotope=\'{"itemSelector": ".entity_listing", "layoutMode": "masonry"}\'>';
    if (empty($entities)) {
	    echo '<p><em>No organizations added yet...</em> <a href="'.site_url('manage/addEntity').'">Add one now.</a></p>';
    } else {

      //Iterate through each organization
    	foreach ($entities as $e) {

        echo '<div class="entity_listing grid-item"><div class="box-header">';
				echo '<h3 class="box-title entity_name">' . $e["name"] . '</h3>';
        echo '<h5 class="entity_category">' . "Organization: " . $categoryOptions[$e["category_id"]] . '</h5>';
				echo '<nav class="account-action-nav">';
				if (!empty($e["website"])) {
					echo ' <a class="btn" href="'.$e["website"].'"><span class="fa fa-external-link"></span> website</a>';
				}
				echo '<a class="btn" href="'.site_url('manage/viewEntity/'.$e["id"]).'"><span class="fa fa-eye"></span> view</a><a class="btn" href="'.site_url('manage/editEntity/'.$e["id"]).'"><span class="glyphicon glyphicon-pencil"></span> edit</a><a class="btn" href="'.site_url('manage/addAccount/0/'.$e["id"]).'"><span class="glyphicon glyphicon-plus"></span> add account</a></nav>';
				if (!empty($e["contact"])) {
					echo "<br/><em>Contact: ".htmlspecialchars($e["contact"])."</em>";
				}
				echo '</div><div class="box-body"><ul>';

        //List all social accounts for current organization
				foreach ($e["accounts"] as $act) {
          $smTypeStr = $smtypeOptions[$act['smtype_id']];
          $acctUrl = $smUrls[$act['smtype_id']].$act['username'];
          echo '<li class="' . strtolower($smTypeStr) . '">';
          echo '<span class="fa fa-' . strtolower($smTypeStr) . '"></span>';

					if ($act['smtype_id'] == 7) {
            echo '<a href="'.$acctUrl.'.tumblr.com" target="_blank">';
					} else {
            echo '<a href="'.$acctUrl.'" target="_blank">';
          }
					if ($act["flagged"] == 1) {
						echo '<strong><em>';
					}
					if ($act["status"] != "active") {
						echo '<del>';
					}
          if (!empty($act["title"])) {
            echo $act["title"].' - ';
          }

					echo $act['username'];

					if ($act["status"] != "active") {
						echo '</del>';
					}
					if ($act["flagged"] == 1) {
						echo '</strong></em>';
					}
					echo '</a>';
					if (isset($act['ticket']) && $act['ticket'] != 0) {
						echo ' - <a href="'.TICKET_URL.$act['ticket'].'">Ticket '.$act['ticket'].'</a> -';
					}
					echo ' <a class="edit-link" href="'.site_url('manage/editAccount/'.$act["id"]).'">(edit)</a>';
					echo '<br/><em>Latest Activity: '.$act['last_updated'].'</em></li>';
				}

				echo '</ul></div></div>';
		}
	}
    ?>

<!-- end grid -->
<?php echo '</div>'; ?>

<?php
$this->load->view("manage/footer.php");
?>
