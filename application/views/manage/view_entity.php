<?php
$this->load->view("manage/header.php");
?>
<div class="box box-solid">
    <?php
		echo '<div class="box-header"><h3 class="box-title">'.$entity['name'].'</h3><a class="btn pull-right" href="'.site_url('manage/editEntity/'.$entity["id"]).'"><span class="glyphicon glyphicon-pencil"></span>edit</a>';
		echo '<p><strong>Category</strong>: '.$categoryOptions[$entity["category_id"]].'<br/>';
		echo '<strong>School</strong>: '.$schoolOptions[$entity["school_id"]].'<br/>';
		echo '<strong>Type</strong>: '.$typeOptions[$entity["type"]]."<br/>";
		if (!empty($entity['website'])) {
			echo '<strong>Website</strong>: <a href="'.$entity['website'].'">'.$entity['website'].'</a><br/>';
		}
		echo "<strong>Contact</strong>: ";
		if (!empty($entity['contact'])) {
			echo htmlspecialchars($entity['contact']);
		} else {
			echo "None";
		}
		echo "</p></div><div class='box-body'>";
		echo '<h4>Accounts</h4>';
		if (empty($accounts)) {
			echo "No associated accounts";
		} else {
			echo '<ul>';
			foreach ($accounts as $act) {				
				echo '<li><a href="'.$smUrls[$act['smtype_id']].$act['username'].'" target="_blank">';
				if ($act["flagged"] == 1) {
					echo '<strong><em>';
				}
				if ($act["status"] != "active") {
					echo '<del>';
				}
				if (!empty($act["title"])) {
					echo $act["title"].' - ';
				}
				echo $smtypeOptions[$act['smtype_id']];
				if ($act["status"] != "active") {
					echo '</del>';
				}
				if ($act["flagged"] == 1) {
					echo '</strong></em>';
				}
				echo '</a>';
				if ($act['ticket'] != 0) {
					echo ' - <a href="'.TICKET_URL.$act['ticket'].'">Ticket '.$act['ticket'].'</a> - ';
				}
				echo ' - Latest Activity: '.$act["last_updated"];
				echo ' <a class="btn" href="'.site_url('manage/editAccount/'.$act["id"]).'"><span class="glyphicon glyphicon-pencil"></span>edit</a>';
				if ($act['status'] == "inactive") {
					echo '<br/><em>'.$act['comments'].'</em>';
				}
				echo '</li>';
				
			}
			echo '</ul><p><a href="'.site_url('manage/editaccount/0/'.$entity["id"]).'"><span class="glyphicon glyphicon-plus"></span> add account</a></p>';
		}
    ?>

<?php
$this->load->view("manage/footer.php");
?>
