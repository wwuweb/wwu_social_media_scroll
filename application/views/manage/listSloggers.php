<?php
$this->load->view("manage/header.php");
?>
<div class="box box-solid">
<div class="box-header">
	<h3 class="box-title">
<?php if (isset($sloggers)) {
	echo "Sloggers";
} else if (isset($revived)) {
	echo "Revived Accounts";
}
?>
	</h3>
</div>
<div class="box-body">
<table class="sortableTable table table-striped"><thead><tr><th>Organization ID</th><th>Email</th><th>Username</th><th>SM Type</th><th>Last Updated</th><th>Type</th><th>Flagged</th><th>Ticket</th></tr></thead>
<tbody>
    <?php
	    $acctList = array();
	    if (isset($sloggers)) {
		    $acctList = $sloggers;
	    } else if (isset($revived)) {
		    $acctList = $revived;
	    }
   foreach ($acctList as $sA) {
	   echo '<tr><td>';
	   echo $sA['entity_id'].'</td><td><a target="_blank" href="'.site_url('statistics/sloggerEmail/'.$sA['entity_id']).'"><span class="fa fa-envelope-o"></span> customized text</a></td></td><td>';
	   echo '<a target="_blank" href="'.$smUrls[$sA['smtype_id']].$sA["username"].'">'.$sA["entity"]["name"].'</a>';
	   if (strpos($sA["username"], "group") !== false) {
		   echo ' (G)';
	   }
	   echo '<br/><a class="btn" href="'.site_url('manage/editAccount/'.$sA["id"]).'" target="_blank"><span class="glyphicon glyphicon-pencil"></span> edit</a>';
	   echo ' <a class="btn" href="'.site_url('manage/viewAccount/'.$sA["id"]).'" target="_blank"><span class="fa fa-eye"></span> view</a>';
	   echo '</td><td>'.$smtypeOptions[$sA['smtype_id']].'</td><td>'.$sA["last_updated"].'</td><td>'.$sA["entity"]["type"].'</td>';
	   echo '<td>'.$sA["flagged"].'</td>';
	   if ($sA["ticket"] != "0") {
		   echo '<td><a target="_blank" href="'.TICKET_URL.$sA["ticket"].'">'.$sA["ticket"].'</a></td>';
	   } else {
		   echo '<td>-</td>';
	   }
	   echo '</tr>';
   }
    ?>
</tbody>
</table>
</div></div>
<?php
$this->load->view("manage/footer.php");
?>
