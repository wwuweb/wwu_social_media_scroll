<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Social Media Directory Manager</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
	<!-- DataTables plugin --->
	<link href="<?php echo base_url("assets/adminlte-plugins/datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url("assets/adminlte-plugins/datatables/dataTables.bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url("assets/adminlte-dist/css/AdminLTE.min.css"); ?>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="<?php echo base_url("assets/adminlte-dist/css/skins/skin-green.min.css"); ?>" rel="stylesheet" type="text/css" />
    <!-- Backend Customization Styles -->
    <link href="<?php echo base_url("css/manage.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body class="skin-green sidebar-mini fixed">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">
	      <!-- Logo -->
        <a href="<?php echo site_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">SmDir</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Social Media Directory</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
	        <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
				<li><a href="<?=site_url("manage/logout") ?>">Logout</a></li>
            </ul>
          </div>
        </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
	        <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
	          
        <li><a href="<?=site_url('manage')?>"><i class="fa fa-home"></i><span>Directory Manager Home</span></a></li>
        <li><a href="<?=site_url('manage/users')?>"><i class="fa fa-user"></i><span>Authorized Users</span></a></li>
        <li><a href="<?=site_url('manage/addEntity/0')?>"><i class="fa fa-plus"></i><span>Add Organization</span></a></li>
        <li><a href="<?=site_url('manage/addAccount/0/0')?>"><i class="fa fa-plus"></i><span>Add Account</span></a></li>
		<li><a href="<?=site_url('statistics')?>"><i class="fa fa-bar-chart"></i><span>Statistics</span></a></li>
		<li><a href="<?=site_url('statistics/sloggers')?>"><i class="fa fa-frown-o"></i><span>Sloggers</span></a></li>
		<li><a href="<?=site_url('statistics/revived')?>"><i class="fa fa-smile-o"></i><span>Revived Accounts</span></a></li>
              <li class="treeview"><a href="#"> <span>Run Status Checks</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
              
	              <li><a href="<?=site_url('statistics/runSloggers/facebook/0/5')?>"><i class="fa fa-facebook-official"></i><span>Check Facebook</span></a></li>
	              <li><a href="<?=site_url('statistics/runSloggers/twitter/0/5')?>"><i class="fa fa-twitter"></i><span>Check Twitter</span></a></li>
	              <li><a href="<?=site_url('statistics/runSloggers/instagram/0/5')?>"><i class="fa fa-instagram"></i><span>Check Instagram</span></a></li>
	              <li><a href="<?=site_url('statistics/runSloggers/youtube/0/5')?>"><i class="fa fa-youtube-play"></i><span>Check YouTube</span></a></li>
	              <li><a href="<?=site_url('statistics/runSloggers/flickr/0/5')?>"><i class="fa fa-flickr"></i><span>Check Flickr</span></a></li>
	              <li><a href="<?=site_url('statistics/runSloggers/tumblr/0/5')?>"><i class="fa fa-tumblr"></i><span>Check Tumblr</span></a></li>
              </ul>
		</li>
		<li><a href="<?=site_url('cache_reset')?>"><span>Refresh Posts</span></a></li>
		<?php if (isset($view_by)) { ?>
        <li class="header">Filter</li>
            <li class="treeview">
              <a href="#"> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
			<li><a href="<?= site_url("manage/listing/all"); ?>">All</a></li>
		<?php
		foreach ($dirTypes as $dT=>$dTdisplay) {
				echo '<li><a href="'.site_url('manage/listing/'.$dTdisplay).'">'.$dTdisplay.'</a></li>';
			}
		?>
              </ul></li>
		<?php
			echo '<li class="treeview">
              <a href="#"> <span>Status</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">';
			echo '<li><a href="'.site_url("manage/listing/".$view_by["type"]."/active").'">Active</a></li>';
			echo '<li><a href="'.site_url("manage/listing/".$view_by["type"]."/inactive").'">Inactive</a></li>';
			echo '<li><a href="'.site_url("manage/listing/".$view_by["type"]."/flagged").'">Flagged</a></li>';
			echo '<li><a href="'.site_url("manage/listing/".$view_by["type"]).'">None</a></li>';
			echo '</li></ul>';
		} ?>
		</ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		<h1>SM Directory Manager
			<?php if (isset($view_by)) {
				echo '<small>'.ucwords($view_by["type"]);
				if (!empty($view_by["filter"])) {
					echo ' '.ucwords($view_by["filter"]);
				}
				echo ' Accounts</small>';
			} ?>
			</h1>
    
	<div id="validation_msg"><?php echo validation_errors(); ?></div>
        </section>
        <!-- Main content -->
        <section class="content">
