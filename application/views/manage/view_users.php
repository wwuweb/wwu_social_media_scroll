<?php
$this->load->view("manage/header.php");

print "<h4>The following CAS users can access the administration area</h4>";
print "<ul>";
foreach ($users as $user) {
	print "<li>".$user['name'];
	if (count($users) > 1 & $user['name'] != $this_user) {
		print " (<a class='deleteThis' href='".site_url("manage/remove_user")."/".$user['id']."'>remove</a>)";
	}
	print "</li>";
}
print "</ul>";

?>

<h4>Add authorized user</h4>

<?php echo form_open('manage/users'); ?>

    <label for="username">CAS Username: </label> <input type="input" name="username" /> <input type="submit" name="submit" value="Authorize user" />

</form>


<?php
$this->load->view("manage/footer.php");
?>