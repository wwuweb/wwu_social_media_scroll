<?php
  //Helper functions for building cards
  function buildProfileURL($social_type, $username) {
    $link = 'http://';
    if(!strcasecmp($social_type, "tumblr")) {
      $link .= $username . '.tumblr.com';
    }
    else {
      $link .= 'www.' . $social_type . '.com/' . $username;
    }
    return $link;
  }
  
  /* Returns the relative time ("5 minutes ago")
   * Modified & optimized version of @zachstronaut's gist, where the epoch times are 
   * precomputed & roughly adjusted for leap years.  
   *
   * Fallback date format: date('F j, Y', $post['date'])
   */
  function getRelativeTime($timestamp) {
    $elapsed  = time() - $timestamp;
    if($timestamp < 1) {
      return '0 seconds ago';
    }
    $times = array( 31556926 => 'year',
                    2629743 => 'month',
                    604800 => 'week',
                    86400 => 'day',
                    3600 => 'hour',
                    60 => 'minute',
                    1 => 'second'   
    );
    foreach ($times as $secs => $t_name) {
      $t = $elapsed / $secs;
      if ($t >= 1) {
        $rel_time  = round($t);
        if($rel_time == 1 && !strcmp($t_name, 'day')) {
          return 'Yesterday at ' . date('g:i A', $timestamp);
        }
        else {
          return $rel_time . ' ' . $t_name . ($rel_time > 1 ? 's' : '') . ' ago';
        }
      }
    }
  }
?>

<?php 
	$this->load->view("header.php"); 
	if (isset($_GET['denied'])) {
		$name= htmlspecialchars($_GET['denied']);
		print '<div id="message">The CAS user '.$name.' does not have access to manage this site.</div>';
	}
?>

<div class="grid">

<?php	
  if($pagenum > 0 && $posts != NULL) {
    $page_indicator = '<div class="page-indicator grid-item"><h2 class="page-number">';
    $page_indicator .= $pagenum;
    $page_indicator .= '</h2><span class="line"></span></div>';
    echo $page_indicator;
  }

	foreach($posts as $post) {
		$card = '	<div class="grid-item ' . strtolower($post["sm_type"]) . "-card" . '">';
        $card .= '<div class="container">';

		if ($post['image']) {
            $image = '<a class="sm-image-wrapper" href="'.$post['post_url'].'">';
            $image .= '<img src="'.$post['image'].'" alt="Click to view on ' . $post["sm_type"] . '" class="sm-image" />';
            $image .= '</a>';
            $card .= $image;
		}

        if ($post['content']) {
            $content = '<div class="content">';
            $content .= $post['content'];
            $content .= '</div>';
            $card .= $content;
        }

        $card_footer = '<div class="entry-footer">';
        $card_footer .= '<span class="post-meta">';
        $card_footer .= '<a class="username" href="' . buildProfileURL($post["sm_type"], $post["user_id"]) . '">' . $post['user_id'] . '</a>';
        $card_footer .= '<div class="post-date">' . getRelativeTime($post["date"]) . '</div>';
        $card_footer .= '</span>';
        $card_footer .= '<a href="'.$post['post_url'].'"><span class="sm-icon"></span><span class="screen-reader-text">'.$post['user_id'].' '.$post["sm_type"].'</span></a>';
        $card_footer .= '</div>';
        $card .= $card_footer;
    
        $card .= '</div>'; //Container
		$card .= '</div>'; //Grid item
		print $card;
	}
?>
</div> <!-- end grid -->

<nav id="pagination">
  <p><?php echo $links; ?></p>
</nav>

<div class="subfooter">
  <h2 class="end-title">That's All, Folks!</h2>
  <h2 class="loading-title">Loading more posts...</h2>
  <span class="line"></span>
  <p class="end-text">
      Thanks for being so interested in what's happening at Western.  Check out our
      <a href="<?php echo site_url("sm_directory") ?>">social media directory</a>; follow us and other groups on campus.
  </p>
</div> <!--end subfooter-->

<?php $this->load->view("footer.php"); ?>
