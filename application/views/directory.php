<?php $this->load->view("header.php"); ?>

<div id="sm-directory" class="set-width">
  <h1 class="title">Social Media Directory</h1>
	<nav>
		<ul id="dirNav">
			<nav id="directory_filter">
				<?php
        $filterIdx = 0;
				foreach ($filterOptions as $foVal=>$foName) {
          echo '<div class="'.'filter_option option-'.($filterIdx+1) . '">';
					echo '<a class="filter_link';
					if ($viewBy == $foVal) {
						echo " active";
					}
					echo '" href="'.site_url('sm_directory/accounts/'.$foVal).'">'.$foName.'</a>';
          echo '</div>';
          $filterIdx++;
				}
				?>
			</nav>
		</ul>
		<!--form id="searchCommunities">
			<input type="text" length="25" name="searchTerm" id="searchTerm" placeholder="Search"/>
			<input type="submit" value="Search" name="searchButton" id="searchButton"/>
			<input type="button" value="Clear" name="clearButton" id="clearButton"/>
		</form-->
	</nav> <!-- end nav -->

<div class="directory-content">
<?php
if (!empty($searchTerm)) {
		echo '<h3>Directory search results for "'.$searchTerm.'"</h3>';
}
	
if ($viewBy == "type") {
	$typeId = -1;
	foreach ($accounts as $aTypeId => $aList) {
		if ($typeId != -1) {
			echo '</ul>';
		}
		if (empty($aList)) {
			continue;
		}
		$typeId = $aTypeId;
		echo "<h3> Accounts on ".$smtypeOptions[$typeId]."</h3>";
		echo '<ul class="entityList">';
		foreach ($aList as $a) {
			$lTitle = $entities[$a["entity_id"]]["name"];
			if (!empty($a["title"])) {
				$lTitle .= ' - '.$a["title"];
			}
			if ($smtypeOptions[$a["smtype_id"]] != "Blog") {
				$lTitle .= ' on';
			}
			$lTitle .= ' '.$smtypeOptions[$a["smtype_id"]];
			echo '<li class="entityItem">';
			echo '<a class="sm_icon sm_icon-'.strtolower($smtypeOptions[$a["smtype_id"]]).'" href="'.$smUrls[$a["smtype_id"]].$a["username"].'" target="_blank" title="'.$lTitle.'" ';
			printGAevent($thisPage, $smtypeOptions[$a["smtype_id"]], $entities[$a["entity_id"]]["name"]);
			echo '></a>';
			echo $entities[$a["entity_id"]]["name"];
            if (!empty($a["title"])) {
                echo " - ".$a["title"];
            }
			echo '</li>';
		}
	}
	echo '</ul>';
} else if ($viewBy == "name") {
	echo "<h3>Accounts by Name</h3>";
	echo '<ul class="entityList">';
	foreach ($entities as $e) {
		echo '<li class="entityItem">'.$e["name"].'<ul class="accountList">';
		printAccount($e, $smUrls, $smtypeOptions, $thisPage);
		echo '</ul></li>';
	}
	echo '</ul>';
} else if ($viewBy == "school") {
	$catId = -1;
	foreach ($entities as $e) {
		if ($catId != $e["school_id"]) {
			if ($catId != -1) {
				echo '</ul>';
			}
			$catId = $e["school_id"];
			echo "<h3> Accounts in ".$schoolOptions[$catId]."</h3>";
			echo '<ul class="entityList">';
		}
		echo '<li class="entityItem">'.$e["name"].'<ul class="accountList">';
		printAccount($e, $smUrls, $smtypeOptions, $thisPage);
		echo '</ul></li>';
	}
	echo '</ul>';
} else { // view by category
	$catId = -1;
	if (!empty($searchTerm)) {
		echo '<ul class="entityList">';
	}
	foreach ($entities as $e) {
		if (empty($searchTerm)) {
			if ($catId != $e["category_id"]) {
				if ($catId != -1) {
					echo '</ul>';
				}
				$catId = $e["category_id"];
				echo '<h3 class="category-name""><a name="' . $shortCategoryOptions[$catId] . '""></a>'.$categoryOptions[$catId]."</h3>";
				echo '<ul class="entityList">';
			}
		}
		echo '<li class="entityItem">'.$e["name"].'<ul class="accountList">';
		printAccount($e, $smUrls, $smtypeOptions, $thisPage);
		echo '</ul></li>';
	}
	echo '</ul>';
} 
?>
<div class="subfooter">
  <span class="line"></span>
  <div class="end-text>">
    <?php if (isset($searchTerm) && empty($entities)) { ?> 
    <p id="no_search_results">Sorry, there are no accounts containing "<?php echo $searchTerm; ?>" in our directory.</p>
    <?php } ?>
    <p>Don't see your account listed or have a correction? Please <a id="directory_submit_link" href="mailto:webhelp@wwu.edu">contact Web Help</a>.</p>
    <?php if (isset($searchTerm) && !empty($searchTerm)) { ?>
    <p id="search_return">Return to the <a id="search_return_link" href="<?php echo site_url('sm_directory/accounts'); ?>">full directory listing</a>.</p>
    <?php } ?>
    </p>
  </div>
</div> <!-- end end-dir-info -->

</div> <!-- end directory-content -->
</div> <!-- end #sm-directory -->

<?php $this->load->view("footer.php"); ?>
