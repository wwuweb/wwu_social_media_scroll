<?php

/*SM Type ID Mappings
 * 1 - Facebook
 * 2 - Twitter
 * 3 - Flickr
 * 4 - YouTube
 * 5 - Vimeo
 * 6 - Instagram
 * 7 - Tumblr
 * 8 - Google +
 * 9 - Pintrest
 * 10 - Linkedin
 */

define("MAX_CACHED_POSTS", 500);

class Tile_model extends CI_Model {

	public function __construct() {
		$this->load->database();
    $this->load->library('Mobile_Detect');
    //Set posts per page based on device type
    $detect = new Mobile_Detect;
    if($detect->isMobile() && !$detect->isTablet()) {
      $post_limit = 25;
    }
    else if($detect->isTablet()) {
      $post_limit = 50;
    }
    else {
      $post_limit = 100;
    }
    //Link post-page limit to user session
    $_SESSION["post_limit"] = $post_limit;
	}
	
	public function get_posts($types = array(1,2,3,4,6,7)) {
		$posts = $this->get_cached_posts();
		if (count($posts) < $_SESSION["post_limit"]) {
			foreach ($types as $type) {
				$posts = array_merge($posts, $this->get_all_posts_of_type($type));
			}
		}
		$posts = $this->sort_posts_by_date($posts);
		return array_slice($posts,0,$_SESSION["post_limit"]);
	}
	
	public function get_cached_posts($limit, $start) {
		$posts = array();
    $this->db->order_by("date", "desc");
    $this->db->limit($limit, $start);
		$query = $this->db->get('posts_cache');
		$smtypes = array('FB'=>'facebook', 'TW'=>'twitter', 'FL'=>'flickr','FS'=>'foursquare', 'IG'=>'instagram', 'LN'=>'linkedin',
					'PN'=>'pinterest', 'TM'=>'tumblr', 'YT'=>'youtube');

    $result = $query->result_array();
    //Prints page limit and offset for debug
    //echo $limit . ' : ' . $start;
    if (is_array($result) && !empty($result)) {
      foreach ($result as $key => $value) {
        $posts[] = array(
          "date" => $result[$key]['date'], 
          "post_id" => $result[$key]['post_id'], 
          "user_id" => $result[$key]['user_id'], 
          "content" => $result[$key]['content'], 
          "post_url" => $result[$key]['post_url'], 
          "image" => isset($result[$key]['image']) && !is_null($result[$key]['image']) ? $result[$key]['image'] : "", 
          "extra_content" => isset($result[$key]['extra_content']) && !is_null($result[$key]['extra_content']) ? $result[$key]['extra_content'] : "", 
          "extra_link" => isset($result[$key]['extra_link']) && !is_null($result[$key]['extra_link']) ? $result[$key]['extra_link'] : "",
          "sm_type" => $smtypes[$result[$key]['sm_type']],
        );
      }
    }
    return $posts;
	}
	
	private function sort_posts_by_date($posts) {
		$smtypes = array('FB'=>'facebook', 'TW'=>'twitter', 'FL'=>'flickr','FS'=>'foursquare', 'IG'=>'instagram', 'LN'=>'linkedin',
					'PN'=>'pinterest', 'TM'=>'tumblr', 'YT'=>'youtube');
		$dates = array();
		foreach ($posts as $key => $value) {
			$dates[$key] = $posts[$key]['date'];
			$posts[$key]['sm_type'] = $smtypes[$posts[$key]['sm_type']];
		}
		array_multisort($dates, SORT_DESC, $posts);
		return $posts;
	}

  //function get_lcpt
  //description: Stands for Latest Cached Post Time.
  //args: social media type ID number, such as '1' for Facebook 
  //Since type_id starts at 1, array must be referenced by type_id-1 to work.
  //returns: time of latest cached post in Unix time format
  private function get_lcpt($type_id) {
    $type_id = $type_id - 1;
    $sm_ids = array('FB', 'TW', 'FL', 'YT', 'VM', 'IG', 'TM', 'GP', 'PN', 'LN');
    $sql = "SELECT sm_type, date FROM posts_cache WHERE sm_type = ? ORDER BY date DESC LIMIT 1";
    $query = $this->db->query($sql, array($sm_ids[$type_id]));
    if($query->num_rows() == 0) {
      return false;
    }
    else {
      $result = $query->row_array();
      $time = $result['date']; 
      echo $sm_ids[$type_id] . " : " . $time ."<br>";
      return $time;
    }
  }

  public function post_cache_count() {
    return $this->db->count_all('posts_cache');
  }

  //function refresh_post_cache
  //description: Adds only new posts to cache.  Does not wipe cache after running.
  //args: array of social media service ids to pull from
  //endpoint: /index.php/cache_refresh (application/config/routes.php) 
  public function refresh_post_cache($types = array(1,2,3,4,6,7)) {
		$new_posts = array();
		foreach ($types as $type) {
      $time = $this->get_lcpt($type);
      if($time) {
        $new_posts = array_merge($new_posts, $this->get_all_posts_of_type($type, $time));
      }
      else {
        $new_posts = array_merge($new_posts, $this->get_all_posts_of_type($type));
      } 
		}
    //Add all new posts instead of emptying cache.
    echo "Added " . count($new_posts) . " new posts to cache\n";
    $this->add_posts_to_cache($new_posts); 
    $post_cache_count = $this->post_cache_count(); 

    //Delete number of oldest posts exceeding MAX_CACHED_POSTS 
    if($post_cache_count > MAX_CACHED_POSTS) {
      echo $num_over_limit . " posts over limit of ". MAX_CACHED_POSTS . ". Removing...\n";
      $num_over_limit = $post_cache_count - MAX_CACHED_POSTS;
      $sql = "DELETE FROM posts_cache ORDER BY date ASC LIMIT ?";
      $query = $this->db->query($sql, array($num_over_limit));
      echo ($query) ? 'Success' : 'Database remove failed';
    }

  }

  //function reset_post_cache
  //description: Grab as many posts as possible & reset post cache
  //args: array of social media service ids to pull from
  //endpoint: /index.php/cache_reset (application/config/routes.php) 
	public function reset_post_cache($types = array(1,2,3,4,6,7)) {
		$posts = array();
		foreach ($types as $type) {
			$posts = array_merge($posts, $this->get_all_posts_of_type($type));
		}

		//If we got enough posts, empty database
		if (count($posts) >= $_SESSION["post_limit"]) {
			$query = $this->db->get('posts_cache');
			$result = $query->result_array();
			if (is_array($result) && !empty($result)) {
				foreach($result as $key => $record) {
					$this->db->where('post_id', $record['post_id']);
					$this->db->delete('posts_cache');
				}
			}
		}
    //Add all grabbed posts to cache
    $this->add_posts_to_cache($posts);
	}

  private function add_posts_to_cache($posts) {
		//Add new posts to database
		foreach ($posts as $post) {
      if($post['post_id'] != NULL) {
        $post['cached_time'] = time();
        $this->db->insert('posts_cache', $post);
      }
		}
  }
	
	private function get_all_posts_of_type($type, $time=0) {
		//Load appropriate library and initialize aggregator
		$this->load->library('SM_Aggregator');
		$aggregator = new SM_Aggregator($type);
		$posts = array();

		//Get all accounts of type
    $sql = "SELECT * FROM accounts a RIGHT JOIN entities e ON a.entity_id=e.id WHERE a.id IS NOT NULL AND a.smtype_id=?";
		$query = $this->db->query($sql, $type);
    $result = $query->result_array();
    $accounts = array();

    //Build array of username=>user_id pairs for aggregator
    foreach ($result as $key => $value) {
      $acct_type = $result[$key]['type'];
      $user_id = $result[$key]['user_id'];
      if(!strcmp($acct_type, "official")) {
        if(!empty($result[$key]['user_id'])) {
          $accounts[$result[$key]['username']] = $result[$key]['user_id'];
        } 
        else { //Attempt to get user_id on the fly if not in database
          $user_id = $result[$key]['username'];
          $new_ids = $aggregator->getIdsFromNames(array($result[$key]['username']));
          if (is_array($new_ids) && in_array($result[$key]['username'], array_keys($new_ids))) {
            $user_id = $new_ids[$result[$key]['username']];
          }
          $accounts[$result[$key]['username']] = $user_id;
        }
      }
   }
   $posts = $aggregator->getPosts($accounts, $time);
     
   return $posts;
  }

}
