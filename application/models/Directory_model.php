<?php
/**
 * Social Media Directory Entries Model
 *
 * @author Creative Services
 */

class Directory_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getCategories() {
		$query = $this->db->get('categories');
        /* categories
        *
        * id
        * short_name
        * full_name
        */
        return $query->result_array();
	}
	
	public function getSMTypes() {
		$query = $this->db->get('smtypes');
        /* smtypes
        *
        * id
        * name
        * base_url
        */
        return $query->result_array();
	}

	public function getSchools() {
		$query = $this->db->get('schools');
        /* schools
        *
        * name
        * id
        */
        return $query->result_array();
	}
	
	public function getEntity($id) {
        $this->db->where("id", $id);
        $this->db->from('entities');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() == 1) {
            $arr = $query->result_array();
            $resultset = $arr[0];
        }
        return $resultset;
	}
    
    public function getEntities($ids=array()) {
        if (!empty($ids)) {
            $this->db->where_in("id", $ids);
        }
		$this->db->order_by("name");
        $this->db->from('entities');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
    
    public function getEntitiesByCategory($c_id) {
        $this->db->where("category_id", $c_id);
        $this->db->from('entities');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
    
    public function getEntitiesByTerm($term) {
	    $this->db->like("name", $term);
        $this->db->from('entities');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
	
	public function getEntitiesByType($t, $viewBy="", $includeOfficial=false) {
		$resultset = array();
		if (!in_array($t, array("all", "official", "officialish", "unofficial"))) {
			return $resultset;
		}
		if ($t != "all") {
			if ($t != "official" && $includeOfficial) {
				$this->db->where_in("type", array("official", $t));
			} else {
				$this->db->where("type", $t);
			}
		}
		
        $this->db->from('entities');
		if (!empty($viewBy)) {
			if ($viewBy == "category") {
				$this->db->order_by("category_id");
			} else if ($viewBy == "school") {
				$this->db->order_by("school_id");
			}
		}
		$this->db->order_by("name");
        $query = $this->db->get();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
	
    public function getEntityFields() {
        return $this->db->list_fields('entities');
    }
    
	public function addEntity($info) {
        $thefields = $this->getEntityFields();
        foreach ($thefields as $afield) {
            if (isset($info[$afield])) {
                $data[$afield] = $info[$afield];
            }
        }
        
        if ($this->db->insert('entities', $data) === FALSE) {
	        $error = $this->db->error();
	        if ($error) {
	            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
	        }
            return -1;
        }
        return $this->db->insert_id();
    }
    
    public function deleteEntity($id) {
	    if (is_numeric($id) && $id > 0) {
	    	$this->db->where("id", $id);
			$this->db->delete("entities");
			if ($this->db->affected_rows() > 0) {
				$this->db->where("entity_id", $id);
				$this->db->delete("accounts");
				return true;
			}
    	}
	    return false;
    }
    
    public function updateEntity($id, $info) {
        $thefields = $this -> getEntityFields();
        foreach ($thefields as $afield) {
            if (isset($info[$afield])) {
                $data[$afield] = $info[$afield];
            }
        }
        $this->db->where("id", $id);
        $this->db->update('entities', $data);
		$error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
            return false;
        }
        return ($this->db->affected_rows() == 1) ? true : false;
    }
	
	public function isUniqueEntityName($name) {
		$this->db->where("name", $name);
		$this->db->from('entities');
		$query = $this->db->get();
		$error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() == 1) {
            return false;
        } else {
			return true;
		}
	}
	
	public function getAccount($id) {
		$this->db->where("id", $id);
        $this->db->from('accounts');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() == 1) {
            $arr = $query->result_array();
            $resultset = $arr[0];
        }
        return $resultset;
	}
    
    public function getAccounts($ids=array(), $status="") {
        if (!empty($ids)) {
            $this->db->where_in("id", $ids);
        }
		if (!empty($status)) {
			if ($status == "flagged") {
				$this->db->where("flagged", 1);	
			} else if ($status == "active") {
				$this->db->where("status", "active");	
			} else if ($status == "inactive") {
				$this->db->where("status", "inactive");	
			}
		}
		$this->db->order_by("entity_id");
		$this->db->order_by("smtype_id");
		$this->db->order_by("title");
        $this->db->from('accounts');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
    
    public function getAccountsByTerm($searchTerm, $status="active") {
		if (!empty($status)) {
			if ($status == "flagged") {
				$this->db->where("flagged", 1);	
			} else if ($status == "active") {
				$this->db->where("status", "active");	
			} else if ($status == "inactive") {
				$this->db->where("status", "inactive");	
			}
		}
		$this->db->order_by("entity_id");
		$this->db->order_by("smtype_id");
		$this->db->order_by("title");
        $this->db->from('accounts');
        $this->db->like("title", $searchTerm);
        $this->db->or_like("username", $searchTerm);
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
	
	public function getFlaggedAccounts($ids=array()) {
		return $this->getAccounts($ids, "flagged");
	}
	public function getActiveAccounts($ids=array()) {
		return $this->getAccounts($ids, "active");
	}
	public function getInactiveAccounts($ids=array()) {
		return $this->getAccounts($ids, "inactive");
	}
	
	public function getAccountsByEntity($eids=array(), $includeInactive=false) {
        if (!empty($eids)) {
            $this->db->where_in("entity_id", $eids);
        }
		if (!$includeInactive) {
			$this->db->where("status", "active");
		}
		$this->db->order_by("entity_id");
		$this->db->order_by("smtype_id");
		$this->db->order_by("title");
        $this->db->from('accounts');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
    
    public function getAccountsWithType($smt_id, $pg=0, $limit=0) {
        $this->db->where("smtype_id", $smt_id);
        $this->db->from('accounts');
        if ($limit > 0) {
	        $this->db->limit($limit, $pg*$limit);
        }
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
    
    public function getAccountsWithTypePaginated($smt_id, $pg, $limit) {
		return $this->getAccountsWithType($smt_id, $pg, $limit);
    }
    
    public function getAccountsForEntity($eid) {
        $this->db->where("entity_id", $eid);
        $this->db->from('accounts');
		$this->db->order_by("smtype_id");
		$this->db->order_by("title");
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
    }
	
	public function getAccountFields() {
        return $this->db->list_fields('accounts');
    }
    
	public function addAccount($info) {
        $thefields = $this->getAccountFields();
        foreach ($thefields as $afield) {
            if (isset($info[$afield])) {
                $data[$afield] = $info[$afield];
            }
        }
        $this->db->insert('accounts', $data);
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
            return -1;
        }
        return $this->db->insert_id();
    }
    
    public function deleteAccount($aId) {
    	if (is_numeric($aId) && $aId > 0) {
	    	$this->db->where("id", $aId);
			$this->db->delete("accounts");
			return true;
    	} else {
	    	return false;
    	}
    }
    
    public function updateAccount($id, $info) {
        $thefields = $this -> getAccountFields();
        foreach ($thefields as $afield) {
            if (isset($info[$afield])) {
                $data[$afield] = $info[$afield];
            }
        }
        $this->db->where("id", $id);
        $this->db->update('accounts', $data);
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
            return false;
        }
        return ($this->db->affected_rows() == 1) ? true : false;
    }
	
	public function countActiveAccounts($id, $inactive=false) {
		$this->db->where("smtype_id", $id);
		if ($inactive) {
			$this->db->where("status", "inactive");
		} else {
			$this->db->where("status", "active");
		}
		$this->db->from('accounts');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return count($resultset);
	}
	
	public function countAccounts() {
		// count each social media channel, separate active and inactive
		$smFull = $this->getSMTypes();
		$results = array();
		foreach($smFull as $smCh) {
			$active = $this->countActiveAccounts($smCh["id"]);
			$inactive = $this->countActiveAccounts($smCh["id"], true);
			$results[$smCh["name"]] = array("active" => $active, "inactive" => $inactive);
		}
		return $results;
		
	}
	public function countEntities() {
		// by type
		$types = array("official", "officialish", "unofficial");
		$results = array();
		foreach ($types as $t) {
			$this->db->where("type", $t);
			$this->db->from('entities');
			$query = $this->db->get();
			$resultset = array();
			$error = $this->db->error();
	        if ($error) {
	            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
	        }
			if ($query->num_rows() > 0) {
				$resultset = $query->result_array();
			}
			$results[$t] = count($resultset);
		}
		return $results;
	}
    public function getFbGroups() {
		$fbId = 2;
		$this->db->where("smtype_id", $fbId);
		$this->db->like("username", "group");
		$this->db->from('accounts');
        $query = $this->db->get();
        $resultset = array();
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
        }
        if ($query->num_rows() > 0) {
            $resultset = $query->result_array();
        }
        return $resultset;
	}
	public function updateAccountTime($id, $lastUpdated) {
        $this->db->where("id", $id);
        $this->db->update('accounts', array("last_updated" => $lastUpdated));
        $error = $this->db->error();
        if ($error) {
            log_message('error','DB Error: '.$error['code'].' - '.$error['message']);
            return false;
        }
        return ($this->db->affected_rows() == 1) ? true : false;
	}
}

?>
