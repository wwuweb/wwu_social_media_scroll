<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		# login check required
		
		$this->data["smtypeOptions"] = $this->getSMTypes();
		$this->data["smUrls"] = $this->getURLs();

		$this->load->library('SM_Aggregator_Twitter');
		$this->load->library('SM_Aggregator_Facebook');
	}
	
	private function getSMTypes() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["name"];
		}
		return $toRet;
	}
	private function getURLs() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["base_url"];
		}
		return $toRet;
	}
	
	public function index() {
		$this->viewStats();
	}
	
	public function sloggers() {
		$accounts = $this->directory_model->getAccounts();
		$sloggerAccounts = array();
		foreach ($accounts as $a) {
			if (($a["last_updated"] < date("Y-m-d", strtotime("-3 months"))) && ($a['status'] == "active")) {
				$eInfo = $this->directory_model->getEntity($a["entity_id"]);
				$info = $a;
				$info["entity"] = $eInfo;
				$sloggerAccounts[] = $info;
			}
		}
		$this->data["sloggers"] = $sloggerAccounts;
		$this->load->view('manage/listSloggers', $this->data);
	}
	
	public function revived() {
		$accounts = $this->directory_model->getAccounts();
		$revivedAccounts = array();
		foreach ($accounts as $a) {
			if (($a["last_updated"] > date("Y-m-d", strtotime("-3 months"))) && ($a['status'] == "inactive")) {
				$eInfo = $this->directory_model->getEntity($a["entity_id"]);
				$info = $a;
				$info["entity"] = $eInfo;
				$revivedAccounts[] = $info;
			}
		}
		$this->data["revived"] = $revivedAccounts;
		$this->load->view('manage/listSloggers', $this->data);
	}
	
	public function sloggerEmail($entityId) {
		$entity = $this->directory_model->getEntity($entityId);
		$accts = $this->directory_model->getAccountsByEntity($entityId);
		$smUrls = $this->getURLs();
		$smTypes = $this->getSMTypes();
		$sloggerAccounts = array();
		foreach ($accts as $a) {
			if (($a["last_updated"] < date("Y-m-d", strtotime("-3 months"))) && ($a['status'] == "active")) {
				$sloggerAccounts[] = $a;
			}
		}
		if (empty($sloggerAccounts)) {
			echo $entity["name"]." accounts are up to date!";
		} else {
			$thisThese = "Is this page";
			$otherPages = "has another newer page taken its place";
			$removeIt = "it";
			if (count($sloggerAccounts) > 1) {
				$thisThese = "Are these pages";
				$otherPages = "have other newer pages taken their place";
				$removeIt = "them";
			}
			echo '<p>'.htmlspecialchars($entity["contact"]).'</p>';
			echo '<p>Hello,<br/>
Periodically our office does a status check on all the social media pages of the organizations, departments, offices, and clubs that we have listed in our social media directory. We seek to keep this list as up to date as possible with active and engaged accounts.</p> 

<p>'.$entity['name'].' has ';
			$ct = 0;
			foreach ($sloggerAccounts as $sA) {
				$ct++;
				$link = $smUrls[$sA['smtype_id']].$sA["username"];
				$actType = $smTypes[$sA['smtype_id']];
				echo "a ".$actType.' account ';
				if ($ct == 1) {
					echo 'on this social media directory';
				}
				echo ' ('.$link.') that has not been updated since '.date("F j, Y", strtotime($sA["last_updated"])).'. ';
				if (count($sloggerAccounts) > 1 && $ct < count($sloggerAccounts)) {
					echo "There is also ";
				}
			}
echo $thisThese.' still actively maintained? Or '.$otherPages.'? If not, we plan to remove '.$removeIt.' from the list so visitors are not directed to an inactive social media page.';
echo'</p>

<p>Any updated information you can provide would be greatly appreciated, or if you can direct me to the best person to contact that would be helpful as well.</p>';	
		}
	}
	
	public function runSloggers($type,$pg=0,$perPg=5) {
		$slogging_accounts = array();
		$miscAccounts = array();
		
		$typeId = 0;
		switch ($type) {
			case "facebook":
				$typeId = 1;
				break;
			case "twitter":
				$typeId = 2;
				break;
			case "instagram":
				$typeId = 6;
				break;
			case "youtube":
				$typeId = 4;
				break;
			case "flickr":
				$typeId = 3;
				break;
			case "tumblr":
				$typeId = 7;
				break;
			default:
			break;
		}
		
		$accounts = $this->directory_model->getAccountsWithTypePaginated($typeId, $pg, $perPg);
		$usernames = array();
		$userinfo = array();
		foreach($accounts as $a) {
			if($a["smtype_id"] == $typeId) {
				$usernames[$a["username"]] = $a["user_id"];
				$userinfo[$a["username"]] = array();
				$userinfo[$a["username"]]['id'] = $a["id"];
				$userinfo[$a["username"]]['last_updated'] = $a["last_updated"];
				$userinfo[$a["username"]]['status'] = $a["status"];
			}
		}
		
		$this->load->library('SM_Aggregator');
		$aggregator = new SM_Aggregator($typeId);
		$latest_post_dates = $aggregator->getLatestPostDates($usernames);
		
		foreach($latest_post_dates as $username => $latest_post_date) {
			$time = date("Y-m-d H:i", $latest_post_date);
			if ($time > $userinfo[$username]['last_updated']) {
				$this->directory_model->updateAccountTime($userinfo[$username]['id'], $time);
			}
			if ($userinfo[$username]['status'] == "inactive") {
				$username .= " (inactive)";
			}
			$slogging_accounts[] = array("username" => $username, "lastUpdate" => $time);
		}
		
		$this->data['current_pg'] = intval($pg) + 1;
		$this->data['per_pg'] = $perPg;
		$this->data['current_type'] = $type;
		$this->data["slogging_accounts"] = $slogging_accounts;
		$this->data["miscAccounts"] = $miscAccounts;
		$this->data["current_sm_type"] = $type;
		$this->load->view('manage/sloggers', $this->data);
	}
	
	public function viewAll() {
		$this->viewEntity(0);
	}
	public function info() {
		echo phpinfo();
	}
	
	public function viewEntity($id) {
		// list all info and accounts
		$this->data["accounts"] = $this->directory_model->getAccountsForEntity($id);
		$this->data["entity"] = $this->directory_model->getEntity($id);
		$this->load->view('manage/view_entity', $this->data);
	}
	
	public function viewStats() {
		$this->data["accounts"] = $this->directory_model->countAccounts();
		$this->data["entities"] = $this->directory_model->countEntities();
		$this->load->view('manage/view_stats', $this->data);
	}
	
}

/* End of file managedirectory.php */
