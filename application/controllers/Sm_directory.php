<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Creative Services, College of William and Mary
*/
class Sm_directory extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->data["categoryOptions"] = $this->getCategories();
		$this->data["shortCategoryOptions"] = $this->getShortCategories();
		$this->data["smtypeOptions"] = $this->getSMTypes();
		$this->data["smUrls"] = $this->getURLs();
		$this->data["schoolOptions"] = $this->getSchools();
		$this->data["filterOptions"] = array("name"=>"Name", "category"=>"Category", "type"=>"Type", "school"=>"School");
		$this->data["title"] = "WWUSocial: Directory";
		$this->data["active"] = "directory";
	}
	
	private function getCategories() {
		$cats = $this->directory_model->getCategories();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["full_name"];
		}
		return $toRet;
	}
	
	private function getShortCategories() {
		$cats = $this->directory_model->getCategories();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["short_name"];
		}
		return $toRet;
	}
	
	private function getSMTypes() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["name"];
		}
		return $toRet;
	}

	private function getURLs() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["base_url"];
		}
		return $toRet;
	}

	private function getSchools() {
		$cats = $this->directory_model->getSchools();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["name"];
		}
		return $toRet;
	}
	public function blogs() {
		$b = new SM_Aggregator_Blog();
		$b->getPosts('http://wmblogs.wm.edu/feed/');
	}
	// Show latest posts
	public function index($viewBy = "")	{
		$this->accounts($viewBy);
	}
	
	public function official($viewBy = "")	{
		//$this->viewDirectoryList("official", $viewBy);
		redirect("/sm_directory/accounts");
	}
	
	public function officialish($viewBy = "") {
		//$this->viewDirectoryList("officialish", $viewBy, true);
		redirect("/sm_directory/accounts");
	}
	
	public function unofficial($viewBy="") {
		//$this->viewDirectoryList("unofficial", $viewBy, false);
		redirect("/sm_directory/accounts");
	}
	
	public function accounts($viewBy="category") {
		$this->viewDirectoryList("all", $viewBy, true);
	}
	
	public function page_missing() {
		$this->load->view('errors/page_missing', $this->data);
	}
	
	public function search($term) {
		if (empty($term)) {
			$term = $this->input->post("searchTerm");
			if (empty($term)) {
				$this->accounts();
				return;
			}
		}
		$term = urldecode($term);
		$resultEntities = $this->directory_model->getEntitiesByTerm($term);
		$eIds = array();
		foreach ($resultEntities as $e) {
			$eIds[] = $e["id"];
		}
		$resultAccounts = $this->directory_model->getAccountsByTerm($term);
		$aeIds = array();
		foreach ($resultAccounts as $e) {
			$aeIds[] = $e["entity_id"];
		}
		$allEntityIds = array_unique(array_merge($aeIds, $eIds));
		
		$eToRet = array();
		if (!empty($allEntityIds)) {
			$es = $this->directory_model->getEntities($allEntityIds);
			
			$as = $this->directory_model->getAccountsByEntity($allEntityIds);
			$aIds = array();
			// create array of accounts by their entity IDs
			foreach ($as as $a) {
				$aIds[$a["entity_id"]][] = $a;
			}
			
			foreach ($es as $e) {
				if (isset($aIds[$e["id"]])) { // handle if there is an entity with no active accounts
					$tmpE = $e;
					$tmpE["accounts"] = $aIds[$e["id"]];
					if (!empty($tmpE["accounts"])) {
						$eToRet[] = $tmpE;	
					}
				}
			}
		}
		$this->data["entities"] = $eToRet;
		$this->data["searchTerm"] = $term;
		$this->load->view('directory', $this->data);
	}
	
	/** Return an alphabetical list of entities with all of their associated accounts, 
		or return the accounts sorted by social media type, entity category or entity school **/
	private function viewDirectoryList($type, $viewBy="", $includeOfficial=true) {
		$this->data["thisPage"] = $type;
		if (!in_array($viewBy, array("name", "category", "type", "school"))) {
			$viewBy = "category";
		}
		$this->data["viewBy"] = $viewBy;
		$es = $this->directory_model->getEntitiesByType($type, $viewBy, $includeOfficial);
		$eIds = array();
		foreach ($es as $e) {
			$eIds[] = $e["id"];
		}
		$as = $this->directory_model->getAccountsByEntity($eIds);
		if ($viewBy == "type") {
			$eById = array();
			// create array of entities indexed by their ID
			foreach ($es as $e) {
				$eById[$e["id"]] = $e;
			}
			$tO = $this->data["smtypeOptions"];
			$actsByC = array();
			// Create array of accounts indexed by social media type
			foreach ($tO as $tId => $tName) {
				$actsByC[$tId] = array();
			}
			foreach ($as as $a) {
				$actsByC[$a["smtype_id"]][$eById[$a["entity_id"]]["name"]] = $a;
			}
			foreach ($actsByC as $aC=>$aList) {
				$sortedList = $aList;
				ksort($sortedList);
				$this->data["accounts"][$aC] = $sortedList;
			}
			$this->data["entities"] = $eById;
		} else {
			$aIds = array();
			// create array of accounts by their entity IDs
			foreach ($as as $a) {
				$aIds[$a["entity_id"]][] = $a;
			}
			$eToRet = array();
			foreach ($es as $e) {
				if (isset($aIds[$e["id"]])) { // handle if there is an entity with no active accounts
					$tmpE = $e;
					$tmpE["accounts"] = $aIds[$e["id"]];
					if (!empty($tmpE["accounts"])) {
						$eToRet[] = $tmpE;	
					}
				}
				
			}
			$this->data["entities"] = $eToRet;
		}
		$this->load->view('directory', $this->data);
	}

}

/* End of file SM_Directory.php */
