<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->data["typeOptions"] = array('' => "--Select One--", 'official' => "Official", 'officialish' => "Official-ish", 'unofficial' => "Unofficial");
		$this->data["accountStatusOptions"] = array('' => "--Select One--", 'active' => "Active", 'inactive' => "Inactive");
		$this->data["categoryOptions"] = $this->getCategories();
		$this->data["smtypeOptions"] = $this->getSMTypes();
		$this->data["dirTypes"] = $this->getDirTypes();
		$this->data["smUrls"] = $this->getURLs();
		$this->data["schoolOptions"] = $this->getSchools();
		
		$this->load->library('session');
		
		if(!$this->session->userdata('logged_in') & current_url() != base_url('index.php/manage/login')) {
			redirect('manage/login', 'refresh');
		}
	}

	private function getCategories() {
		$cats = $this->directory_model->getCategories();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["full_name"];
		}
		return $toRet;
	}
	
	private function getSMTypes() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["name"];
		}
		return $toRet;
	}
	
	private function getSchools() {
		$cats = $this->directory_model->getSchools();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["name"];
		}
		return $toRet;
	}
	
	private function getURLs() {
		$cats = $this->directory_model->getSMTypes();
		$toRet = array();
		foreach ($cats as $c) {
			$toRet[$c["id"]] = $c["base_url"];
		}
		return $toRet;
	}
	
	private function getDirTypes() {
		return array("official", "officialish", "unofficial");
	}
	
	private function getEntityOptions() {
		$entities = $this->directory_model->getEntities();
		$toRet = array();
		foreach ($entities as $e) {
			$toRet[$e["id"]] = $e["name"];
		}
		asort($toRet);
		return $toRet;
	}
	
	public function index() {
		$this->listing();
	}
	
	/** Return a list of entities with all of their associated accounts, 
		or limited by entity type or account status if specified **/
	public function listing($type="", $filter="") {
		$entities = array();
		if (in_array($type, array("officialish", "official", "unofficial"))) {
			$entities = $this->directory_model->getEntitiesByType($type, "", false);
		} else {
			$type = "all";	
			$entities = $this->directory_model->getEntities();
		}
		if (empty($entities)) {
			$this->data["entities"] = array();
		} else {
			$accounts = array();
			if (!empty($filter) && in_array($filter, array("flagged", "active", "inactive"))) {
				if ($filter == "flagged") {
					$accounts = $this->directory_model->getFlaggedAccounts();
				} else if ($filter == "active") {
					$accounts = $this->directory_model->getActiveAccounts();
				} else if ($filter == "inactive") {
					$accounts = $this->directory_model->getInactiveAccounts();
				}
			} else {
				$accounts = $this->directory_model->getAccounts();	
			}
			$eToRet = array();
			foreach ($entities as $e) {
				$eToRet = $e;
				$acts = array();
				foreach ($accounts as $a) {
					if ($a["entity_id"] == $e["id"]) {
						$acts[] = $a;
					}
				}
				$eToRet["accounts"] = $acts;
				$this->data["entities"][] = $eToRet;
			}
		}
		$this->data["view_by"] = array("type" => $type, "filter" => $filter);
		$this->data["entity_type"] = $type;
		$this->load->view('manage/main', $this->data);
	}
	
	public function viewAll() {
		$this->viewEntity(0);
	}
	
	public function viewEntity($id) {
		// list all info and accounts
		$this->data["accounts"] = $this->directory_model->getAccountsForEntity($id);
		$this->data["entity"] = $this->directory_model->getEntity($id);
		$this->load->view('manage/view_entity', $this->data);
	}
	
	public function addEntity() {
		$this->editEntity(0);
	}
	
	public function deleteEntity($id) {
		if (is_numeric($id) && $id > 0) {
			$this->directory_model->deleteEntity($id);
		}
		$this->listing();
	}
	
	public function editEntity($id=0) {
		if ($id != 0) {
			$this->data["entity"] = $this->directory_model->getEntity($id);
		} else {
			$this->data["entity"] = array("id" => 0, "name" => "", "type" => "", "category_id" => "", "school_id" => "", "contact" => "", "website" => "");
		}
		$this->load->view('manage/edit_entity', $this->data);
	}
	
	public function saveEntity($id=0) {
		$this->form_validation->set_rules('id', "Entity ID", "trim|numeric|required");
		$this->form_validation->set_rules('category_id', "Category ID", "trim|numeric|required");
		$this->form_validation->set_rules('school_id', "School ID", "trim|numeric|required");
		$this->form_validation->set_rules('type', "Type", "required");
		if ($id == 0) {
			$this->form_validation->set_rules('name', "Entity Name", "trim|required|callback_unique_entity_name");
		} else {
			$this->form_validation->set_rules('name', "Entity Name", "trim|required");
		}

		if ($this->form_validation->run() == FALSE) {
            $this->editEntity();
			return;
        }
        // do update
        else {
            $toSave = array("name" => $this->input->post("name"),
							"type" => $this->input->post("type"),
							"category_id" => $this->input->post("category_id"),
							"school_id" => $this->input->post("school_id"),
							"contact" => $this->input->post("contact"),
							"website" => $this->input->post("website"));
			$eId = $this->input->post("id");
			if ($eId == 0) {
				$ret = $this->directory_model->addEntity($toSave);
				$toSave["id"] = $ret;
			} else {
				$ret = $this->directory_model->updateEntity($eId, $toSave);
				$toSave["id"] = $eId;
			}
			redirect('/manage/viewEntity/'.$toSave["id"]);
		}
	}
	
	public function viewAccount($id) {
		$this->data["account"] = $this->directory_model->getAccount($id);
		$this->data["entity"] = $this->directory_model->getEntity($this->data["account"]["entity_id"]);
		$this->load->view('manage/view_account', $this->data);
	}
	
	public function addAccount($aId=0, $eId=0) {
		$this->editAccount($aId, $eId);
	}
	
	public function deleteAccount($aId, $eId) {
		if (is_numeric($aId) && $aId != 0) {
			$this->directory_model->deleteAccount($aId);
		}
		$this->viewEntity($eId);
	}
	
	public function editAccount($aId=0, $eId=0) {
		if ($aId != 0) {
			$this->data["account"] = $this->directory_model->getAccount($aId);
			if ($this->data["account"]["entity_id"] != 0) {
				$this->data["entity"] = $this->directory_model->getEntity($this->data["account"]["entity_id"]);
			}
		} else {
			$this->data["account"] = array("id" => $aId, "entity_id" => $eId, "smtype_id" => "", "last_updated" => "0000-00-00", "status" => "", "username" => "", "title" => "", "comments" => "", "ticket" => 0, "flagged" => 0);
			if ($eId != 0) {
				$this->data["entity"] = $this->directory_model->getEntity($eId);
			}
		}
		$this->data["entityOptions"] = $this->getEntityOptions();

		$this->load->view('manage/edit_account', $this->data);
	}
	
	public function saveAccount($id=0) {
		$this->form_validation->set_rules('id', "Account ID", "trim|numeric|required");
		$this->form_validation->set_rules('entity_id', "Entity ID", "trim|numeric|required");
		$this->form_validation->set_rules('smtype_id', "Social Media Type", "trim|numeric|required");
		$this->form_validation->set_rules('last_updated', "Latest Activity", "required");
		$this->form_validation->set_rules('status', "Status", "required");
		$this->form_validation->set_rules('username', "Username/URL", "trim|required");
		$this->form_validation->set_rules('title', "Title", "trim");
		$this->form_validation->set_rules('comments', "Comments", "trim");
		$this->form_validation->set_rules('ticket', "Ticket ID", "trim|numeric");
		$this->form_validation->set_rules('flagged', "Flagged", "trim|numeric");
		if ($this->form_validation->run() == FALSE) {
            $this->editAccount();
			return;
        }
        // do update
        else {
            $toSave = array("entity_id" => $this->input->post("entity_id"),
							"smtype_id" => $this->input->post("smtype_id"),
							"status" => $this->input->post("status"),
							"username" => $this->input->post("username"),
							"title" => $this->input->post("title"),
							"comments" => $this->input->post("comments"),
							"ticket" => $this->input->post("ticket"),
							"flagged" => $this->input->post("flagged"),
							"last_updated" => $this->input->post("last_updated"));
			//Get user_id from username (which differ for Flickr and YouTube)
			$toSave["user_id"] = $toSave["username"];
			$this->load->library('SM_Aggregator');
			$aggregator = new SM_Aggregator($toSave['smtype_id']);
			$user_ids = $aggregator->getIdsFromNames(array($toSave["username"]));
			if (is_array($user_ids) && in_array($toSave["username"], array_keys($user_ids))) {
				$toSave["user_id"] = $user_ids[$toSave["username"]];
			}
			$aId = $this->input->post("id");
			if ($aId == 0) {
				$ret = $this->directory_model->addAccount($toSave);
				$toSave["id"] = $ret;
			} else {
				$ret = $this->directory_model->updateAccount($aId, $toSave);
				$toSave["id"] = $aId;
			}
			redirect('/manage/viewEntity/'.$toSave["entity_id"]);
		}
	}
	
	public function viewStats() {
		$this->data["accounts"] = $this->directory_model->countAccounts();
		$this->data["entities"] = $this->directory_model->countEntities();
		$this->load->view('manage/view_stats', $this->data);
	}
	
	public function unique_entity_name($name) {
		$isUnique = $this->directory_model->isUniqueEntityName($name);
		if(!$isUnique) {
			$this->form_validation->set_message('unique_entity_name', 'The entity "'.$name.'" is already in the directory');
			return false;
		} else {
			return true;
		} 
	}
	
	public function login() {
		print("Redirecting to Univeral Login...");
		$this->load->library('cas');
		$this->cas->force_auth();
		$name = $this->cas->user()->userlogin;
    	$this->load->database();
    	$query = $this->db->query('SELECT * FROM users WHERE name="'.$name.'"');
    	if ($query->num_rows() < 1) {
        	//CAS authenticated user is not allowed access, don't log in and redirect to home page
        	redirect('/?denied='.$name, 'refresh');
        }
        $this->session->set_userdata('logged_in', array('username'=>$name));
        redirect('manage', 'refresh');
	}
	
	public function logout() {
		//$this->cas->logout();
		print("Logging out...");
		$this->session->unset_userdata('logged_in');
   		session_destroy();
   		redirect('/', 'refresh');
	}
	
	private function getUsers() {
		$query = $this->db->get('users');
        return $query->result_array();
	}
	
	public function users() {
		$this->load->helper('form');
    	$this->load->library('form_validation');

    	$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.name]');

    	if (!($this->form_validation->run() === FALSE)) {
    		$query = $this->db->query('INSERT INTO users (name) VALUES("'.$this->input->post('username').'")');
    	}
    	
    	$data['this_user'] = $this->session->userdata('logged_in')['username'];
		$data['users'] = $this->getUsers();
		$this->load->view('manage/view_users', $data);
	}
	
	public function remove_user($id) {
		$users = $this->getUsers();
		foreach ($users as $user) {
			if ($user['id'] == $id) {
				$query = $this->db->query('DELETE FROM users WHERE id='.$id);
				break;
			}
		}
		redirect('/manage/users');
	}
}

/* End of file managedirectory.php */
