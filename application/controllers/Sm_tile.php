<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sm_tile extends CI_Controller {

    public function __construct() {
	parent::__construct();
        $this->load->helper('url');
	$this->load->model('Tile_model');
        $this->load->library('pagination');
    }

    public function view($page="") {
        $config = array();
        $config['base_url'] = base_url() . 'index.php/page'; 
        $config['total_rows'] = $this->Tile_model->post_cache_count();
        $config['per_page'] = $_SESSION['post_limit'];
        $config['uri_segment'] = 2;
        $config['next_tag_open'] = '<div class=pagination-next>';
        $config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);

        if ($page == "cache_reset") {
	    $this->Tile_model->reset_post_cache();
	    redirect('manage', 'refresh');
        }
        elseif($page == "cache_refresh") {
            $this->Tile_model->refresh_post_cache();
	    redirect('manage', 'refresh');
        }
        else {
	    //$posts = $this->Tile_model->get_posts();
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2): 0;
            $offset = $page == 0 ? 0 : ($page-1)*$config['per_page'];

            $data['posts'] = $this->Tile_model->get_cached_posts($config['per_page'], $offset);
            $data['links'] = $this->pagination->create_links();
	    $data['title'] = "WWUSocial";
	    $data['active'] = "scroll";
            $data['pagenum'] = $page;
	    $this->load->view('tile', $data);
	}
    }
}
