<?php

class SM_Aggregator_Flickr {
	
	public function getPosts($usernames, $cutoff_time=0) {
		$flickr_objs = SM_Aggregator_Flickr::getJSON($usernames);
		$toret = array();
		if (!empty($flickr_objs)) {
			foreach($flickr_objs as $username => $flickr_obj) {
				foreach ($flickr_obj->photos->photo as $post) {
					$item = array(
						"date" => $post->dateupload, 
						"post_id" => $post->id, 
						"user_id" => $username, 
						"content" => "", 
						"post_url" => 'http://www.flickr.com/photos/'.$post->owner.'/'.$post->id, 
						"image" => $post->url_m, 
						"extra_content" => "", 
						"extra_link" => "",
						"sm_type" => "FL"
					);
          //If current post is older than passed time, skip it.
          if ($item["date"] <= $cutoff_time) {
            continue;
          }
					if (isset($post->title)) {
						$item["content"] = $post->title;
					}
					if (isset($post->description) && isset($post->description->content) && !empty($post->description->content)) {
						$item["extra_content"] = $post->description->content;
					}
			
					$toret[] = $item;
				}
			}
		}
		return $toret;		
	}
	
	//http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
	public function getIdsFromNames($usernames){
		$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
		foreach ($usernames as $username) {
			$curly[$username] = curl_init();
			$apiCall = "https://api.flickr.com/services/rest/?method=flickr.urls.lookupUser&api_key=".FL_API_KEY."&url=https://www.flickr.com/photos/".$username."&format=json";
			$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
		}
		
		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(SM_Aggregator_Flickr::extractJSON(curl_multi_getcontent($c)))->user->id;
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

	//http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
    		$apiCall = "https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=".FL_API_KEY."&user_id=".$id."&extras=date_upload,description,url_m&per_page=25&format=json";
    		$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(SM_Aggregator_Flickr::extractJSON(curl_multi_getcontent($c)));
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }
    
    private function extractJSON($string) {
    	$p = '/({.*})/';
    	if(preg_match($p, $string, $matches)) {
    		return $matches[0];
    	} else {
    		return "";
    	}
    }

}
?>
