<?php

require_once( 'SM_Aggregator_Facebook.php' );
require_once( 'SM_Aggregator_Flickr.php' );
require_once( 'SM_Aggregator_Instagram.php' );
require_once( 'SM_Aggregator_Tumblr.php' );
require_once( 'SM_Aggregator_Twitter.php' );
require_once( 'SM_Aggregator_YouTube.php' );

class SM_Aggregator {

	private $aggregator = NULL;
	public $sm_type = 0;
	
	public function __construct($sm_type_id="") {
		$this->sm_type = $sm_type_id;
		switch ($sm_type_id) {
			case 2:
				$this->aggregator = new SM_Aggregator_Twitter();
				break;
			case 3:
				$this->aggregator = new SM_Aggregator_Flickr();
				break;
			case 4:
				$this->aggregator = new SM_Aggregator_YouTube();
				break;
			case 6:
				$this->aggregator = new SM_Aggregator_Instagram();
				break;
			case 7:
				$this->aggregator = new SM_Aggregator_Tumblr();
				break;
			default:
				$this->aggregator = new SM_Aggregator_Facebook();
				break;
		}
	}
	
	public function getLatestPostDates($usernames) {
		$posts = $this->aggregator->getPosts($usernames);
		$user_posts = array();
		foreach ($posts as $post) {
			if (!in_array($post['user_id'], array_keys($user_posts))) {
				$user_posts[$post['user_id']] = $post['date'];
			} elseif ($post['date'] > $user_posts[$post['user_id']]) {
				$user_posts[$post['user_id']] = $post['date'];
			}
		}
		return $user_posts;
	}
	
	public function getIdsFromNames($userids) {
		return $this->aggregator->getIdsFromNames($userids);
	}
	
	public function getPosts($userids, $cutoff_time=0) {
		return $this->aggregator->getPosts($userids, $cutoff_time);
	}
}

?>
