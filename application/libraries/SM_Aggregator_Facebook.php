<?php

class SM_Aggregator_Facebook {
	
	public function getPosts($userids, $cutoff_time=0) {
		$post_arrays = SM_Aggregator_Facebook::getJSON($userids);
		$toret = array();
		if(!empty($post_arrays)) {
			foreach ($post_arrays as $username => $post_array) {
				$posts = $post_array->data;
				if (!empty($posts)) {
					foreach ($posts as $post) {
						if (isset($post->message)) {
							$postId = explode("_", $post->id);
							$item = array(
								"date" => strtotime($post->created_time),
								"post_id" => $postId[1],
								"user_id" => $username,
								"content" => $post->message,
								"post_url" => 'https://www.facebook.com/' . $postId[0] . '/posts/' . $postId[1],
								"image" => "",
								"extra_content" => "",
								"extra_link" => "",
								"sm_type" => "FB"
							);
							//If current post is older than passed time, skip it.
							if ($item["date"] <= $cutoff_time) {
								continue;
							}
							if (isset($post->full_picture)) {
								$item["image"] = $post->full_picture;
							} elseif (isset($post->picture)) {
								$item["image"] = $post->picture;
							}
							if (isset($post->link)) {
								$item["extra_link"] = $post->link;
								if (isset($post->full_picture)) {
									$item["image"] = $post->full_picture;
								}
								if (isset($post->object_id) && ($post->type == "photo")) {
									$item["image"] = "https://graph.facebook.com/" . $post->object_id . "/picture?type=normal";
								}
								if (isset($post->name)) {
									$item["extra_content"] = $post->name;
								}
							}
							$toret[] = $item;
						}
					}
				}
			}
		}
		return $toret;
	}
	
	public function getIdsFromNames($usernames) {
		$userids = array();
		foreach ($usernames as $username) {
			$userids[$username] = $username;
		}
		return $userids;
	}

	public function formatToken($jsonData) {
		$result = "";

		$tokens = preg_split('","', $jsonData);
		foreach ($tokens as $key => $value) {
			$value = str_replace('"', "", $value);
			$value = str_replace('}', "", $value);
			$value = str_replace('{', "", $value);
			$value = str_replace(':', "=", $value);
			$result = strlen($result) > 0 ? $result . "&" . $value : $result = $value;
		}

		return $result;
	}
	
	private function getAccessToken() {
		$apiCall = "https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=client_credentials";
        $headerData = array('Accept: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiCall);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $jsonData = curl_exec($ch);
        // split header from JSON data
        // and assign each to a variable
        list($headerContent, $jsonData) = explode("\r\n\r\n", $jsonData, 2);

        curl_close($ch);
		$jsonData = $this->formatToken($jsonData);
        return $jsonData; //returns string "access_token=..."
	}
	
	//http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
    	$access_token_string = SM_Aggregator_Facebook::getAccessToken();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
			$apiCall = "https://graph.facebook.com/".$id."/posts?".$access_token_string."&fields=picture,full_picture,message,link,created_time";
			$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c));
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

}
?>
