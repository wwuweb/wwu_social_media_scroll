<?php

class SM_Aggregator_Tumblr {

	public function getPosts($userids, $cutoff_time=0) {
		$json_objs = SM_Aggregator_Tumblr::getJSON($userids);
		$toret = array();
		if (!empty($json_objs)) {
			foreach ($json_objs as $username => $json) {
				foreach ($json->response->posts as $post) {
					//var_dump($post);
					$type = $post->type;
					$allowed_post_types = array("text","chat","quote","link","answer","photo");
					if (!in_array($type, $allowed_post_types)) {
						continue; //Not worth dealing with embedded audio and video
					}
					$item = array(
						"date" => $post->timestamp, 
						"post_id" => $post->id, 
						"user_id" => $post->blog_name, 
						"content" => "",
						"post_url" => $post->post_url, 
						"image" => "", 
						"extra_content" => "", 
						"extra_link" => "",
						"sm_type" => "TM"
					);
          //If current post is older than passed time, skip it.
          if ($item["date"] <= $cutoff_time) {
            continue;
          }
					switch($type) {
						case 'text':
							$title = isset($post->title) & !empty($post->title) ? $post->title.'<br />' : "";
							$body = isset($post->body) & !empty($post->body) ? $post->body : "";
							$item['content'] = $title.$body;
							break;
						case 'chat':
							$title = isset($post->title) & !empty($post->title) ? $post->title.'<br />' : "";
							$body = isset($post->body) & !empty($post->body) ? $post->body : "";
							$item['content'] = $title.$body;
							break;
						case 'quote':
							$text = isset($post->text) & !empty($post->text) ? $post->text : "";
							$source = isset($post->source) & !empty($post->source) ? $post->source : "";
							$item['content'] = $text.'<br />'.$source.'';
							break;
						case 'link':
							$title = isset($post->title) & !empty($post->title) ? $post->title.'<br />' : "";
							$url = isset($post->url) & !empty($post->url) ? $post->url.'<br />' : "";
							if ($url != "" & $title != "") {
								$url = '<a href="'.$url.'">'.$title.'</a>';
							} else {
								$url = '<a href="'.$url.'">'.$url.'</a>'; 
							}
							$description = isset($post->description) & !empty($post->description) ? $post->description : "";
							$item['content'] = $url.'<br />'.$description;
							break;
						case 'answer':
							$asking_name = isset($post->asking_name) & !empty($post->asking_name) ? $post->asking_name : "";
							$asking_url = isset($post->asking_url) & !empty($post->asking_url) ? $post->asking_url : "";
							if ($asking_name != "" & $asking_url != "") {
								$asking_name = '<a href="'.$asking_url.'">'.$asking_name.'</a>';
							}
							$question = isset($post->question) & !empty($post->question) ? $post->question : "";
							$answer = isset($post->answer) & !empty($post->answer) ? $post->answer : "";
							$item['content'] = $asking_name.': '.$question.'<br /><br />'.$answer;
							$image = isset($post->photos[0]->alt_sizes[1]->url) & !empty($post->photos[0]->alt_sizes[0]->url) ? $post->photos[0]->alt_sizes[0]->url : "";
							$item['image'] = $image;
							break;
						case 'photo':
							$caption = isset($post->caption) & !empty($post->caption) ? $post->caption : "";
							$item['content'] = $caption;
							$image = isset($post->photos[0]->alt_sizes[1]->url) & !empty($post->photos[0]->alt_sizes[0]->url) ? $post->photos[0]->alt_sizes[0]->url : "";
							$item['image'] = $image;
							break;
					}
					$toret[] = $item;
				}
			}
		}
		return $toret;
	}
	
	public function getIdsFromNames($usernames) {
		$userids = array();
		foreach ($usernames as $username) {
			$userids[$username] = $username;
		}
		return $userids;
	}
    
    //http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
    		$apiCall = "https://api.tumblr.com/v2/blog/".$id."/posts?api_key=".TM_API_KEY;
    		$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c));
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

}
?>
