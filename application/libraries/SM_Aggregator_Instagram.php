<?php

class SM_Aggregator_Instagram {
	
	function linkify_ig_caption($caption) {
	 
	  //Convert urls to <a> links
	  $caption = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $caption);
	 
	  //Convert at mentions to IG profiles in <a> links
	  $caption = preg_replace("/@([A-Za-z0-9_]*)/", "<a href=\"http://www.instagram.com/$1\">@$1</a>", $caption);
	 
	  //Convert hashtags to IG explore links in <a> links
	  $caption = preg_replace("/#([A-Za-z0-9_]*)/", "<a href=\"http://www.instagram.com/explore/tags/$1\">#$1</a>", $caption);
	 
	  return $caption;
	 
	}
	
	public function getPosts($userids, $cutoff_time=0) {
		$ig_json_objs = SM_Aggregator_Instagram::getJSON($userids);
		$toret = array();
		if (!empty($ig_json_objs)) {

				foreach ($ig_json_objs as $username => $ig_json) {
					if (!is_null($ig_json)) {
						foreach ($ig_json->items as $post) {
							$item = array(
								"date" => $post->created_time,
								"post_id" => $post->id,
								"user_id" => $post->user->username,
								"content" => "",
								"post_url" => $post->link,
								"image" => $post->images->standard_resolution->url,
								"extra_content" => "",
								"extra_link" => "",
								"sm_type" => "IG"
							);
							//If current post is older than passed time, skip it.
							if ($item["date"] <= $cutoff_time) {
								continue;
							}
							if ($post->caption != null) {
								$item["content"] = SM_Aggregator_Instagram::linkify_ig_caption($post->caption->text);
							}

							$toret[] = $item;
						}
					}
				}

		}
		return $toret;
	}
	
	public function getIdsFromNames($usernames) {
		$userids = array();
		foreach ($usernames as $username) {
			$userids[$username] = $username;
		}
		return $userids;
	}
    
    //http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
    		$apiCall = "https://www.instagram.com/".$id."/media/";
    		$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c));

			if ($result[$id]->status == "ok") {
				echo("Got good status");
			} else {
				echo("Not good:");
			}

    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

}
?>
