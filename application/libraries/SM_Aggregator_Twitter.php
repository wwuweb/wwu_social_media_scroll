<?php

class SM_Aggregator_Twitter {
	
	/**
	 * Hyperlinks hashtags, twitter names, and urls within the text of a tweet
	 * 
	 * @param object $apiResponseTweetObject A json_decoded() one of these: https://dev.twitter.com/docs/platform-objects/tweets
	 * @return string The tweet's text with hyperlinks added
	 */
	private function linkEntitiesWithinText($apiResponseTweetObject) {
	
            // Split characters on 
            $characters = preg_split('//u', $apiResponseTweetObject->text, -1, PREG_SPLIT_NO_EMPTY);
            
	    // Check if this is a web RT, if so, copy in RT text rather than truncated version
		if (isset($apiResponseTweetObject->retweeted_status)) {
				$text = 'RT @'.$apiResponseTweetObject->retweeted_status->user->screen_name.': '.$apiResponseTweetObject->retweeted_status->text;
				$characters = str_split($text);
		}    

	    // Insert starting and closing link tags at indices...
	
	    // ... for @user_mentions
	    if (isset($apiResponseTweetObject->entities->user_mentions)) {
		    foreach ($apiResponseTweetObject->entities->user_mentions as $entity) {
		        $link = "https://twitter.com/" . $entity->screen_name;          
		        $characters[$entity->indices[0]] = "<a href=\"$link\">" . $characters[$entity->indices[0]];
		        $characters[$entity->indices[1] - 1] .= "</a>";         
		    }               
	    }
	
	    // ... for #hashtags
	    if (isset($apiResponseTweetObject->entities->hashtags)) {
		    foreach ($apiResponseTweetObject->entities->hashtags as $entity) {
		        $link = "https://twitter.com/search?q=%23" . $entity->text;         
		        SM_Aggregator_Twitter::linkify($characters, $link, $entity);     
		    }
		}
		
	    // ... for http://urls
	    if (isset($apiResponseTweetObject->entities->urls)) {
		    foreach ($apiResponseTweetObject->entities->urls as $entity) {
		        $link = $entity->expanded_url;          
                        SM_Aggregator_Twitter::linkify($characters, $link, $entity);
		    }
	    }
	
	    // ... for media
	    if (isset($apiResponseTweetObject->entities->media)) {
		    foreach ($apiResponseTweetObject->entities->media as $entity) {
		        $link = $entity->expanded_url;          
		        SM_Aggregator_Twitter::linkify($characters, $link, $entity);
		    }
	    }

	    // Convert array back to string
	    return implode('', $characters);
	}
	
	private function between($val, $min, $max) {
		return $val >= $min && $val <= $max;
	}
        
        private function linkify(&$characters, $link, $entity) {        
            $start = $entity->indices[0];
            $end = $entity->indices[1];
            $characters[$start] = "<a href=\"$link\">" . $characters[$start];
            $characters[$end - 1] .= "</a>"; 
        }
	
	/*
	 * Given the start and end indices of a URL, hashtag, etc. provided by the Twitter API (which are unreliable),
	 * find the actual start (inclusive) and end (exclusive) indices of the URL.
	 *
	 * @param character array $char_array Array of characters from the Tweet's text.
	 * @param character $start_char The character that the linked string should start with (e.g. '#' for hashtags 'h' for links).
	 * @param int $hint_start_ix The starting index provided by Twitter.
	 * @param int $hint_end_ix The end index provided by Twitter.
	 * @return int array The actual start index (at key 'start_ix') and end index (at key 'end_ix').
	 *
	 */
	private function findIndices($char_array, $start_char, $hint_start_ix, $hint_end_ix) {
		//Default to given indices if worse comes to worst
		$first_word_start_ix = $hint_start_ix;
		$first_word_end_ix = $hint_end_ix;
		$second_word_start_ix = $hint_start_ix;
		$second_word_end_ix = $hint_end_ix;
	
		//Split the "string" into words by finding the indices of all spaces + beginning and end of string
		$space_ixs = array_keys($char_array, ' ');
		array_unshift($space_ixs, -1);
		array_push($space_ixs, count($char_array));
		
		//Find the words that contain the given start and/or end indices
		for($i = 0; $i < count($space_ixs) - 1; $i++) {
			$word_start_ix = $space_ixs[$i] + 1;
			$word_end_ix = $space_ixs[$i + 1];
			if (SM_Aggregator_Twitter::between($hint_start_ix, $word_start_ix, $word_end_ix) && 
					SM_Aggregator_Twitter::between($hint_end_ix, $word_start_ix, $word_end_ix)) {
				$first_word_start_ix = $word_start_ix;
				$first_word_end_ix = $word_end_ix;
				$second_word_start_ix = $word_start_ix;
				$second_word_end_ix = $word_end_ix;
				break;
			} elseif(SM_Aggregator_Twitter::between($hint_start_ix, $word_start_ix, $word_end_ix)) {
				$first_word_start_ix = $word_start_ix;
				$first_word_end_ix = $word_end_ix;
			} elseif (SM_Aggregator_Twitter::between($hint_end_ix, $word_start_ix, $word_end_ix)) {
				$second_word_start_ix = $word_start_ix;
				$second_word_end_ix = $word_end_ix;
			}
		}
		
		//Chose which word needs to be linked, prefer second word (which may be same as first) if tie
		$indices = array();
		if ($char_array[$first_word_start_ix] == $start_char && 
				$char_array[$first_word_start_ix] != $char_array[$second_word_start_ix]) {
			$indices['start_ix'] = $first_word_start_ix;
			$indices['end_ix'] = $first_word_end_ix - 1;
		} else {
			$indices['start_ix'] = $second_word_start_ix;
			$indices['end_ix'] = $second_word_end_ix - 1;
		}
		
		return $indices;
	}
	
	public function getPosts($userids, $cutoff_time=0) {
		$tw_json_objs = SM_Aggregator_Twitter::getJSON($userids);
		$toret = array();
		if (!empty($tw_json_objs)) {
			foreach ($tw_json_objs as $tw_json) {
				foreach ($tw_json as $tweet) {
					$item = array(
						"date" => strtotime($tweet->created_at), 
						"post_id" => $tweet->id_str, 
						"user_id" => $tweet->user->screen_name, 
						"content" => SM_Aggregator_Twitter::linkEntitiesWithinText($tweet), 
						"post_url" => 'http://www.twitter.com/'.$tweet->user->screen_name.'/status/'.$tweet->id_str, 
						"image" => "", 
						"extra_content" => isset($tweet->user->profile_image_url_https) && !empty($tweet->user->profile_image_url_https) ? $tweet->user->profile_image_url_https : "", 
						"extra_link" => "",
						"sm_type" => "TW"
					);
                                        //If current post is older than passed time, skip it.
                                        if ($item["date"] <= $cutoff_time) {
                                          continue;
                                        }
					if (!empty($tweet->entities->media)) {
						$item['extra_link'] = $tweet->entities->media[0]->url;
						$item['image'] = $tweet->entities->media[0]->media_url_https;
					}
	
					$toret[] = $item;
				}
			}
		}
		return $toret;
	}
	
	public function getIdsFromNames($usernames) {
		$userids = array();
		foreach ($usernames as $username) {
			$userids[$username] = $username;
		}
		return $userids;
	}
	
	private function getAccessToken() {
		$token_credentials = urlencode(TW_CONSUMER_KEY).':'.urlencode(TW_CONSUMER_SECRET);
		$encoded_credentials = base64_encode($token_credentials);
		$apiCall = "https://api.twitter.com/oauth2/token";
        $headerData = array(
        	'Authorization: Basic '.$encoded_credentials,
        	'Content-Type:application/x-www-form-urlencoded;charset=UTF-8',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiCall);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');

        $jsonData = json_decode(curl_exec($ch));
        curl_close($ch);

        return $jsonData->access_token;
	}
	
	//http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$access_token = SM_Aggregator_Twitter::getAccessToken();
    	$mh = curl_multi_init();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
    		$apiCall = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$id."&count=35&include_rts=1&exclude_replies=1";
    		$headerData = array(
    			'Accept: application/json',
    			'Authorization: Bearer '.$access_token,
    		);
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curly[$username], CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curly[$username], CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_setopt($curly[$username], CURLOPT_ENCODING, 'gzip');
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c));
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

}
?>
