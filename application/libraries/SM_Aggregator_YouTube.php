<?php

class SM_Aggregator_YouTube {
	
	public function getPosts($uploadedIds, $cutoff_time=0) {
		$toret = array();
		$yt_arrays = SM_Aggregator_YouTube::getJSON($uploadedIds);
		if (!empty($yt_arrays)) {
			foreach ($yt_arrays as $username => $yt_array) {
				foreach ($yt_array->items as $video) {
					$vid_id = "";
					$vid_url = "";
					if (isset($video->id->videoId)) {
						$vid_id = $video->id->videoId;
						$vid_url = 'http://www.youtube.com/watch?v='.$vid_id;
					} elseif (isset($video->id->playlistId)) {
						$vid_id = $video->id->playlistId;
						$vid_url = 'https://www.youtube.com/playlist?list='.$vid_id;
					}
					$item = array(
						"date" => strtotime($video->snippet->publishedAt), 
						"post_id" => $vid_id, 
						"user_id" => $username, 
						"content" => $video->snippet->title, 
						"post_url" => $vid_url, 
						"image" => $video->snippet->thumbnails->medium->url, 
						"extra_content" => $video->snippet->description, 
						"extra_link" => "",
						"sm_type" => "YT"
					);
          //If current post is older than passed time, skip it.
          if ($item["date"] <= $cutoff_time) {
            continue;
          }
					$toret[] = $item;
				}
			}
		}
		return $toret;
	}
	
	//http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
	public function getIdsFromNames($usernames){
		$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
		foreach ($usernames as $username) {
			$curly[$username] = curl_init();
			$apiCall = "https://www.googleapis.com/youtube/v3/channels?forUsername=".$username."&part=id,snippet,contentDetails,statistics,topicDetails,invideoPromotion&key=".YT_API_KEY;
			$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
		}
		
		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c))->items[0]->id;
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }
    
    //http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
    protected function getJSON($userids) {
    	$curly = array();
    	$result = array();
    	$mh = curl_multi_init();
    	foreach ($userids as $username => $id) {
    		$curly[$username] = curl_init();
    		$apiCall = "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=".$id."&maxResults=25&key=".YT_API_KEY;
    		$headerData = array('Accept: application/json');
    		curl_setopt($curly[$username], CURLOPT_URL, $apiCall);
        	curl_setopt($curly[$username], CURLOPT_HTTPHEADER, $headerData);
        	curl_setopt($curly[$username], CURLOPT_CONNECTTIMEOUT, 20);
        	curl_setopt($curly[$username], CURLOPT_TIMEOUT, 90);
        	curl_setopt($curly[$username], CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($curly[$username], CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($curly[$username], CURLOPT_HEADER, false);
        	curl_multi_add_handle($mh, $curly[$username]);
 		}
 		
 		$running = null;
  		do {
    		curl_multi_exec($mh, $running);
  		} while($running > 0);
 		
 		foreach($curly as $id => $c) {
    		$result[$id] = json_decode(curl_multi_getcontent($c));
    		curl_multi_remove_handle($mh, $c);
  		}
  		
  		curl_multi_close($mh);
 
  		return $result;
    }

}
?>
