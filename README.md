#Social Media Directory v1.2.0
__Western Washington University // WebTech__

>Based on the project by The College of William & Mary
>http://www.wm.edu/creative
>creative@wm.edu

See license.txt for license information

This is a simple database-driven application to manage and maintain a large list of social media accounts, particularly for institutions of higher education.

Setup
======
To begin, you will need:

1. a server capable of running PHP5
2. a SQL database server

###Set Base URL
CodeIgniter requires a URL to be set. If you are setting this up locally, you will need to define where you are accessing the site. Go to /application/config/config.php and change line 20 to match your installation path such as ```$config['base_url'] = 'http://localhost:8080/wwu_social_media_scroll';```

###Set up the database
1. Create a database named sm_directory with  ```mysqladmin create sm_directory```
2. Create a user with read and write permissions to the sm_directory database
3. Import the schema using ```mysql -u user -p <foo> < sm_dir_database_setup.sql sm_directory```
4. Once created, add the database connection information to lines 67-70 of /application/config/database.php

###Add in Social Media API Keys
In ```application/config```, create a copy of ```sm_keys.sample.php```, name it ```sm_keys.php``` and fill in the 'xxx' with the appropriate values from each site.  

* Facebook: <https://developers.facebook.com/docs/reference/php>
* Twitter: <https://github.com/J7mbo/twitter-api-php>
* Instagram: <https://github.com/cosenary/Instagram-PHP-API>
* YouTube: <https://github.com/madcoda/php-youtube-api>
* Flickr: <https://github.com/dan-coulter/phpflickr>

###Install Dependencies

__Front-end build utilities__
We use npm, Grunt and Bundler to manage our front-end dependencies.  

1. Make sure you have NodeJS and NPM installed.  If not, follow the [directions on Node's website](https://nodejs.org/en/download/package-manager/).  You may also need to install nodejs-legacy.  
2. If this is the first time you've used Grunt, you will need to install  ```grunt-cli``` by running ```sudo npm install -g grunt-cli```.
3. In the root directory, run ```npm install```.  This step may take a while.
4. Next, run ```gem install bundler``` to install bundler.  Run ```bundle install``` afterwards to install the required Ruby packages.
5. Once the install is finished, just type  ```grunt``` into your command line to run the build script.  If you need to compile for production with minified scripts & stylesheets, run ```grunt build``` instead.
6. All done!  Pull up your site and go to the next section to start adding social media accounts.  


Managing the directory
======================
To access the management back-end of the directory (the "directory manager"), go to http://<your URL>/index.php/manage.

## Checking for latest social media posts and "sloggers"
Accounts that have not posted for more than three (3) months are considered "sloggers" (slacking on posting). You can run status checks on the Facebook, Twitter, Instagram or YouTube accounts in your database via the "Run Status Checks" menu items. Due to API restrictions the number of accounts checked at one time is defaulted to 5 (can be changed by updating the accounts per page parameter on the checking URL, ex. changing the '5' in https://localhost:8888/socialdirectory/index.php/statistics/runSloggers/facebook/0/5 to '10' https://localhost:8888/socialdirectory/index.php/statistics/runSloggers/facebook/0/10) and you will need to page incrementally through your accounts to check all of them.

##Administrative Access
NOTE: Out of the box, the access permission to the directory manager is a simple shared password (defaulted to "hello") so ANYONE can edit the directory if they know the password. You are HIGHLY ENCOURAGED to use a more structured user login to control access to your directory, either tied in with your school's login system (like LDAP) or via a simple database-stored username and password like the examples below:

   1. <http://www.codefactorycr.com/login-with-codeigniter-php.html>
   2. <http://www.codeproject.com/Articles/476944/Create-user-login-with-Codeigniter>

##Notes on Terminology
###Organizations
Each organization (programmatically know as an "entity") can have multiple social media presences ("accounts"), including multiple accounts on the same channel.

An organization is classified by three properties, the school it is affiliated with, the type of organization in relation to the institution ("type") and a general categorization of the kind of organization ("category").
NOTE: The "type" is defined in the database as an enumerated fields by default as "official," "officialish" and "unofficial" but this can be customized to whatever you prefer. If you do customize, be sure to update the "getDirTypes" function in  /application/models/Directory_model.php to reflect the new types.

###Accounts
An account is always associated with an organization. If the name of the account is something other than the name of the entity (for example, a blog title), that can be entered in the "Title" field of the account.

You are encouraged to review the social media accounts in your directory periodically to check on their activity, the "Review Status" section for an Account keep track of these checks. An account can be flagged for review (it still appears in the public list, but is bolded and italicized in the administrative side for easy finding), as well as have an associated help ticket ID (if you are using a help-ticket system to track communication with the account's contact) and general comment area.

An account can be active (it appears in the public-facing directory listing), inactive (it does not appear in the public-facing directory listing but is listed with a strikethrough in the directory manager), or flagged for review (it appears in the public-facing directory listing but is bolded and italicized in the directory manager). You can filter accounts by status in the directory manager.

##Libraries & Frameworks Used
* CodeIgniter: <http://www.codeigniter.com/user_guide/index.html>
* Masonry JS: <http://masonry.desandro.com/>
* EmojiOne Conversion Library: <http://emojione.com/>
* Velocity JS Animation: <http://julian.com/research/velocity/>
