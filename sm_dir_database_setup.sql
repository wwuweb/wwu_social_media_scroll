SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: 'social'
--

-- --------------------------------------------------------
--
-- Table structure for table 'accounts'
--

CREATE TABLE accounts (
  id int(11) NOT NULL AUTO_INCREMENT,
  entity_id int(11) NOT NULL,
  smtype_id int(11) NOT NULL,
  status enum('active','inactive') NOT NULL,
  username varchar(255) NOT NULL,
  user_id varchar(255) NOT NULL,
  title varchar(255) NOT NULL,
  comments varchar(1024) NOT NULL,
  ticket int(11) NOT NULL DEFAULT '0',
  flagged tinyint(1) NOT NULL DEFAULT '0',
  last_updated date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table 'categories'
--

CREATE TABLE categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  short_name varchar(64) NOT NULL,
  full_name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

--
-- Dumping data for table 'categories'
--

INSERT INTO categories VALUES(1, 'university', 'University');
INSERT INTO categories VALUES(2, 'college', 'Colleges & Institutes');
INSERT INTO categories VALUES(3, 'academic', 'Academic Departments & Centers');
INSERT INTO categories VALUES(4, 'administrative', 'Administrative Offices & Groups');
INSERT INTO categories VALUES(5, 'libraries', 'Libraries & Collections');
INSERT INTO categories VALUES(6, 'athletics', 'Athletic Programs & Organizations');
INSERT INTO categories VALUES(7, 'class', 'Class Groups');
INSERT INTO categories VALUES(8, 'residence', 'Residence Halls');
INSERT INTO categories VALUES(9, 'arts-org', 'Arts Organizations');
INSERT INTO categories VALUES(10, 'academic-org', 'Academic Programs & Organizations');
INSERT INTO categories VALUES(11, 'student', 'Student Clubs');
INSERT INTO categories VALUES(12, 'media-org', 'Media Organizations');
INSERT INTO categories VALUES(13, 'as', 'Associated Students');
INSERT INTO categories VALUES(14, 'misc', 'Other');

-- --------------------------------------------------------

--
-- Table structure for table 'entities'
--

CREATE TABLE entities (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  type enum('official','officialish','unofficial') NOT NULL,
  category_id int(11) NOT NULL,
  school_id int(11) NOT NULL DEFAULT '12',
  contact varchar(255) NOT NULL,
  website varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY name (name)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table 'schools'
--

CREATE TABLE schools (
  name varchar(255) NOT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table 'schools'
--

INSERT INTO schools VALUES('Western Washington University', 1);
INSERT INTO schools VALUES('College of Business and Economics', 2);
INSERT INTO schools VALUES('College of Fine and Performing Arts', 3);
INSERT INTO schools VALUES('College of Humanities and Social Sciences', 4);
INSERT INTO schools VALUES('College of Science and Engineering', 5);
INSERT INTO schools VALUES('Fairhaven College', 6);
INSERT INTO schools VALUES('Huxley College', 7);
INSERT INTO schools VALUES('Woodring College', 8);
INSERT INTO schools VALUES('Non-academic', 9);
INSERT INTO schools VALUES('Other', 10);

-- --------------------------------------------------------

--
-- Table structure for table 'smtypes'
--

CREATE TABLE smtypes (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  base_url varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table 'smtypes'
--

INSERT INTO smtypes VALUES(1, 'Facebook', 'http://www.facebook.com/');
INSERT INTO smtypes VALUES(2, 'Twitter', 'http://www.twitter.com/');
INSERT INTO smtypes VALUES(3, 'Flickr', 'http://www.flickr.com/');
INSERT INTO smtypes VALUES(4, 'YouTube', 'http://www.youtube.com/');
INSERT INTO smtypes VALUES(5, 'Vimeo', 'http://www.vimeo.com/');
INSERT INTO smtypes VALUES(6, 'Instagram', 'http://www.instagram.com/');
INSERT INTO smtypes VALUES(7, 'Tumblr', 'http://');
INSERT INTO smtypes VALUES(8, 'Google+', 'http://plus.google.com/');
INSERT INTO smtypes VALUES(9, 'Pinterest', 'http://www.pinterest.com/');
INSERT INTO smtypes VALUES(10, 'LinkedIn', 'http://www.linkedin.com/');

-- --------------------------------------------------------

--
-- Table structure for table 'users'
--

CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table 'users'
--

INSERT INTO users VALUES(1, 'lommena');
INSERT INTO users VALUES(2, 'bronsem');
INSERT INTO users VALUES(3, 'ander278');
INSERT INTO users VALUES(4, 'takemom');
INSERT INTO users VALUES(5, 'waltria');

-- --------------------------------------------------------

--
-- Table structure for table 'posts_cache'
--

CREATE TABLE posts_cache (
  id int(11) NOT NULL AUTO_INCREMENT,
  sm_type varchar(2) NOT NULL,
  date int(64) NOT NULL,
  post_id int(64) NOT NULL,
  user_id varchar(64) NOT NULL,
  content text NOT NULL,
  post_url varchar(200) NOT NULL,
  image varchar(200),
  extra_content text, 
  extra_link varchar(200),
  cached_time int(64) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

--
-- Table structure for 'settings'
--

CREATE TABLE options (
  option_id bigint(11) NOT NULL AUTO_INCREMENT,
  option_name varchar(64) NOT NULL,
  option_value longtext NOT NULL,
  UNIQUE (option_name),
  PRIMARY KEY (option_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
