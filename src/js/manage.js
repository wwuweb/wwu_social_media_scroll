$(document).ready(function(){
	$(".deleteThis").click(function(e){ 
	    if (confirm("Are you sure you want to delete this?")) {
		    return true;
	    } else {
	    	e.preventDefault();
		    return false;
	    }
    });
    
    $(".sortableTable").dataTable( {
	 	"bPaginate": false,   
    });
});