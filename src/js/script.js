window.onload = function() {
  twemoji.parse(document.body);
};

$(document).ready(function() {
  var grid = $('.grid');

  grid.isotope({
    "itemSelector": ".grid-item", 
    "columnWidth": ".grid-item", 
    "percentPosition": true
  });

  grid.imagesLoaded().progress( function() {
    grid.isotope('layout');
  });

  grid.infinitescroll({
    navSelector: '#pagination',
    nextSelector: '#pagination .pagination-next a',
    itemSelector: '.grid .grid-item',
    loadingText: '', 
    loadingImg: '',
    donetext: '', 
    debug: false,
    errorCallback: function() {
      $('.loading-title').hide();
      $('.end-title').show();
      $('.end-text').show();
    }
  },
  //Isotope callback
  function(newEls) {
    var $newCards = $(newEls).css({ opacity: 0 });
    $newCards.imagesLoaded(function() {
      $newCards.animate({ opacity: 1 });
      grid.isotope('appended', $newCards);
    });
  });
});

