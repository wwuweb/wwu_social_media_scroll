$(document).ready(function() {
  //--------------------- WESTERN SEARCH ---------------------//
  var searchButton = $(".western-search > button");
  var searchWidget = $(".western-search-widget");
  var navLink = $(".sm-nav-link");

  function openSearch() {
    searchWidget.velocity({width: 370}, "easeOutQuint");
    searchButton.addClass("is-open");
    $(".western-search-terms > input:nth-child(1)").focus();
    navLink.velocity({blur: 3}, 'easeOutCubic');
  }

  function closeSearch() {
    searchWidget.velocity("reverse");
    searchButton.removeClass("is-open");
    navLink.velocity("reverse", 0.1);
  }

  /* select the search box and animate open */
  searchButton.toggle(openSearch, closeSearch);

});
