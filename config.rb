require 'breakpoint'
require 'zen-grids'

environment = :development
#environment = :production

sass_dir        = "src/sass"
css_dir         = "css"
images_dir      = "img"
javascripts_dir = "js"

relative_assets = true

# Set the output style based on settings above
output_style = (environment == :development) ? :expanded : :compressed
